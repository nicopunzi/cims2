from openerp.osv import fields, osv
from tools.translate import _
import logging
import datetime
import time

_logger = logging.getLogger(__name__)
_debug=False

class product_product(osv.Model):
    _inherit = 'product.product'
    
    _columns = {
        'is_parts' : fields.boolean('Parts'),
    }