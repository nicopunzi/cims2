from openerp.osv import fields, osv
from tools.translate import _
import logging
import datetime

w_types = [('warning','Warning'),('info','Information'),('error','Error')]

class isf_lib_warning(osv.osv_memory):
    _name = 'isf.lib.warning'
    
    _columns = {
        'type': fields.selection(w_types, string='Type', readonly=True),
        'title': fields.char('Title',size=32,readonly=True),
        'message': fields.text('Message',size=1024,readonly=True),
    }
    _req_name = 'title'

    def _get_view_id(self, cr, uid):
        res = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'isf_lib', 'isf_lib_warning_form')
        if res:
            return res[1]
        else:
            return False

    def message(self, cr, uid, id, context):
        message = self.browse(cr, uid, id)
        message_type = [t[1]for t in w_types if message.type == t[0]][0]
        res = {
            'name': '%s: %s' % (_(message_type), _(message.title)),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': self._get_view_id(cr, uid),
            'res_model': 'isf.lib.warning',
            'domain': [],
            'context': context,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'res_id': message.id
        }
        return res

    def warning(self, cr, uid, title, message, context=None):
        id = self.create(cr, uid, {'title': title, 'message': message, 'type': 'warning'})
        res = self.message(cr, uid, id, context)
        return res

    def info(self, cr, uid, title, message, context=None):
        id = self.create(cr, uid, {'title': title, 'message': message, 'type': 'info'})
        res = self.message(cr, uid, id, context)
        return res

    def error(self, cr, uid, title, message, context=None):
        id = self.create(cr, uid, {'title': title, 'message': message, 'type': 'error'})
        res = self.message(cr, uid, id, context)
        return res
        
isf_lib_warning()