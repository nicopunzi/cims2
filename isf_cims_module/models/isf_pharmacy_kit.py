from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from openerp.tools.translate import _
from openerp.osv import fields, osv
from openerp import tools
import logging
import datetime
import openerp
from lxml import etree

_logger = logging.getLogger(__name__)
_debug=False

class isf_pharmacy_kit_line(osv.Model):
    _name = 'isf.pharmacy.kit.line'
    
    _columns = {
        'product' : fields.many2one('product.product','name','Product',required=True),
        'qty' : fields.integer('Quantity', required=True),
        'price' : fields.float('Unit Price', required=True),
    }

    def onchange_product(self, cr, uid, ids, product_id):
        prod_obj = self.pool.get('product.product')
        prod_ids = prod_obj.search(cr, uid, [('id','=',product_id)])
        prod = prod_obj.browse(cr, uid, prod_ids)[0]

        result = {'value' : {}}
        result['value'].update({
            'price' : prod.list_price,
            })

        return result

class isf_pharmacy_kit(osv.Model):
    _name = 'isf.pharmacy.kit'
    
    def _compute_price(self, cr, uid, ids, field_name, arg, context=None):
        res = {} 
        for data in self.browse(cr, uid, ids, context=context):
            price = 0
            for line in data.product_line_ids:
                if _debug:
                    _logger.debug('Price : %f', line.product.list_price)
                price += line.product.list_price * line.qty
            res[data.id] = price
            
        return res
    
    _columns = {
        'name' : fields.char('Kit Name', required=True),
        'product_line_ids' : fields.many2many('isf.pharmacy.kit.line',string="Kit Line", required=True),
        'kit_price' : fields.function(_compute_price,type="float",string="Kit Price"),
    }