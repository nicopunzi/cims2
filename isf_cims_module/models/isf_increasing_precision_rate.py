import time
from lxml import etree

from openerp import netsvc
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _
from openerp.tools import float_compare
from openerp.report import report_sxw
import logging

_logger = logging.getLogger(__name__)


class res_currency(osv.osv):
    _inherit = "res.currency"

    def _current_rate(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = super(res_currency, self)._current_rate(cr, uid, ids, name, arg, context=context)
        if context.get('voucher_special_currency') in ids and context.get('voucher_special_currency_rate'):
            res[context.get('voucher_special_currency')] = context.get('voucher_special_currency_rate')
        return res

    _columns = {
        # same definition of rate that in base in order to just overwrite the function
        'rate': fields.function(_current_rate, string='Current Rate', digits=(20,16),
            help='The rate of the currency to the currency of rate 1.'),
    }

res_currency()

class account_voucher(osv.osv):
	_inherit = "account.voucher"
	
	_columns = {
		'payment_rate': fields.float('Exchange Rate', digits=(20,16), required=True, readonly=True, states={'draft': [('readonly', False)]},
		help='The specific rate that will be used, in this voucher, between the selected currency (in \'Payment Rate Currency\' field)  and the voucher currency.'),
	}
	
account_voucher()