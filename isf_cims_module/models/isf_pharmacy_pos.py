from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from openerp.tools.translate import _
from openerp.osv import fields, osv
from openerp import tools
from openerp import netsvc
import logging
import datetime
import openerp
from lxml import etree

_logger = logging.getLogger(__name__)
_debug=False


class account_voucher(osv.Model):
    _inherit = 'account.voucher'
    

    def button_proforma_voucher(self, cr, uid, ids, context=None):
        res = super(account_voucher,self).button_proforma_voucher(cr, uid, ids, context=context)
        voucher = self.browse(cr, uid, ids)[0]
        ref = voucher.reference
         
        now = datetime.datetime.now()
        date = str(now.year)+'-'+str(now.month)+'-'+str(now.day)
                     
        invoice_id = context.get('invoice_id')
        inv_obj = self.pool.get('account.invoice')
        pos_obj = self.pool.get('isf.pharmacy.pos')
        pos_ids = pos_obj.search(cr, uid, [('invoice_id','=',invoice_id)])
         
        inv_obj.write(cr, uid, invoice_id, {'document_number' : ref}, context=context)
        for pos in pos_obj.browse(cr, uid, pos_ids):
            pos_obj.write(cr, uid, pos.id, {'name' : ref}, context=context)
            pos_obj.write(cr, uid, pos.id, {'state' : 'close' }, context=context)
            pos_obj.write(cr, uid, pos.id, {'payment_date' : voucher.date }, context=context)
            #pos_obj.write(cr, uid, pos.id, {'date' : voucher.date }, context=context)
 
        pos_obj = self.pool.get('isf.pos')
        pos_ids = pos_obj.search(cr, uid, [('invoice_id','=',invoice_id)])
        for pos in pos_obj.browse(cr, uid, pos_ids):
            #pos_obj.write(cr, uid, pos.id, {'name' : ref}, context=context)
            pos_obj.write(cr, uid, pos.id, {'state' : 'confirm' }, context=context)
            pos_obj.write(cr, uid, pos.id, {'payment_date' : voucher.date }, context=context)
            #pos_obj.write(cr, uid, pos.id, {'date' : voucher.date }, context=context)
        
        return res
    
class isf_pharmacy_product_line(osv.osv):
    _name = 'isf.pharmacy.product.line'
    
    _rec_name = 'product'
    
    def _get_mult(self, p_type):
        mult = 1.0
        if p_type =='foc':
            mult = 0.0
        return mult
    
    
    def _get_allowed_categories(self, cr, uid):
        ir_values = self.pool.get('ir.values')
        product_category_ids = ir_values.get_default(cr, uid, 'isf.pharmacy.pos', 'product_category_ids')
        
        return product_category_ids
    
    def _compute_total_amount(self, cr, uid, ids, context=None):
            line = self.browse(cr, uid, ids)[0]
            return line.price * line.amount
    
    def _total_computed(self, cr, uid, ids, field_name, arg, context=None):
        data = self.browse(cr, uid, ids)[0]
        res = {}
        res[data.id] = data.amount * data.price
        
        return res
    
    def _get_stock_location_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','stock_location_id')
        
    def _stock_available(self, cr, uid, ids,field_name, arg, context):
        location_id = self._get_stock_location_id(cr, uid)
        stock_o = self.pool.get('stock.production.lot')
        ctx = context.copy()
        ctx.update({
            'location_id' : location_id,
        })
        res = {}
        for line_id in ids:
            line = self.browse(cr, uid, line_id)
            if _debug:
                _logger.debug('LINE : %s,%d,%s,%s',line,line.id,line.amount,line.product.id)
            
            stock_ids = stock_o.search(cr, uid, [('product_id','=',line.product.id)],context=ctx)
            available_qty = 0.0
            for stock in stock_o.browse(cr, uid, stock_ids):
                if _debug:
                    _logger.debug('STOCK : %s',stock)
                available_qty += stock.stock_available
            
            res[line_id] = available_qty
            
        return res
        
    def _product_stock_available(self, cr, uid, product_id, context):
        location_id = self._get_stock_location_id(cr, uid)
        stock_o = self.pool.get('stock.production.lot')
        ctx = context.copy()
        ctx.update({
            'location_id' : location_id,
        })
        res = {}
       
        stock_ids = stock_o.search(cr, uid, [('product_id','=',product_id)],context=ctx)
        available_qty = 0.0
        for stock in stock_o.browse(cr, uid, stock_ids):
            available_qty += stock.stock_available
            
        return available_qty
        
    
    _columns = {
        'product' : fields.many2one('product.product','name','Product',required=True),
        'amount' : fields.integer('Qty', required=True),
        'price' : fields.float('Price', required=True),
        'total_computed' : fields.function(_total_computed,type="float",string='Total', store=True),
        'state' : fields.selection([('draft','Draft'), ('credit','Credit'), ('close','Close')], 'Status', readonly=True),
        'stock_available' : fields.function(_stock_available,type="integer",string="Stock Available"),
    }
    
    _defaults = {
        'amount' : 1.0,
        'state' : 'draft',
    }
    
   
    
    def set_unavailable(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state':'unavailable'}, context=context)
        return True
        
    def set_available(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state':'ok'}, context=context)
        return True
    
    def onchange_product(self, cr, uid, ids, product_id, amount, context=None):
        if context is None:
            context = {}
            
        p_type = context.get('p_type')
        mult = self._get_mult(p_type)
        
        if not product_id:
            cat_ids = self._get_allowed_categories(cr, uid)
            return {'domain':{'product':[('categ_id','in',cat_ids)]}}
            
        product_obj = self.pool.get('product.product')
        product_ids = product_obj.search(cr, uid, [('id','=',product_id)])
        product = product_obj.browse(cr, uid, product_ids)[0]
        
        total_computed = product.list_price * amount * mult
        
        stock_available = self._product_stock_available(cr, uid, product.id, context=context)
        
        if _debug:
            _logger.debug('=> IDS: %s STOCK : %s', ids,stock_available)
        
        result = {'value': {} }
        result['value'].update({
            'price' : product.list_price,
            'total_computed' : total_computed,
            'stock_available' : stock_available, 
        })
        
        self.write(cr, uid, ids, {'total_computed': total_computed}, context)
            
        return result
        
    def onchange_amount(self, cr, uid, ids, price, amount, context=None):
        p_type = context.get('p_type')
        mult = self._get_mult(p_type)
        total_computed = price * amount * mult
        result = {'value': {} }
        result['value'].update({
            'total_computed' : total_computed,
        })
        
        self.write(cr, uid, ids, {'total_computed': total_computed}, context)
        
        return result
        
    def onchange_price(self, cr, uid, ids, price, amount, context=None):
        p_type = context.get('p_type')
        mult = self._get_mult(p_type)
        total_computed = price * amount * mult
        result = {'value': {} }
        result['value'].update({
            'total_computed' : total_computed,
        })
        
        self.write(cr, uid, ids, {'total_computed': total_computed}, context)
        
        return result
        
isf_pharmacy_product_line()

class isf_pharmacy_pos(osv.osv):
    _name = 'isf.pharmacy.pos'
    _description = 'ISF Pharmacy POS Interface'
    
    def _get_mult(self, p_type):
        mult = 1.0
        if p_type =='foc':
            mult = 0.0
        return mult
    
    
    def _calculate_discount(self, cr, uid, ids, field_name, arg, context=None):
        total_amount = 0.0
        res = {}
        discount = 0.0
        
        for data in self.browse(cr, uid, ids, context=context):
            mult = self._get_mult(data.type)
            total_amount = 0.0
            for line in data.product_line_ids:
                 total_amount = total_amount + line.price * mult * line.amount
            if data.total_paid != 0.0:
                discount = total_amount - data.total_paid
            res[data.id] = discount
        return res
     
    
    def _calculate_total(self, cr, uid, ids, field_name, arg, context=None):
        if _debug:
            _logger.debug('Calculate Total')
        total_amount = 0.0
        res = {}
        discount = 0.0
        for data in self.browse(cr, uid, ids, context=context):
            mult = self._get_mult(data.type)
            total_amount = 0.0
            for line in data.product_line_ids:
                 total_amount = total_amount + line.price * mult * line.amount
            if data.total_paid != 0.0:
                discount = total_amount - data.total_paid
            res[data.id] = total_amount - discount
        return res
        
    def _calculate_free(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        
        for data in self.browse(cr, uid, ids, context=context):
            total_amount = 0.0
            if data.type == 'foc':
                for line in data.product_line_ids:
                     total_amount = total_amount + line.price * line.amount
            res[data.id] = total_amount
            
        return res
                
    
    def _penalty(self, cr, uid, ids, field_name, arg, context=None):
        if not ids: return {}
        res = {}
        for id in ids:
            res[id] = 20.0

        for line in self.browse(cr, uid, ids, context=context):
            if line.picking_id:
                if line.picking_id.purchase_id:
                    if line.picking_id.purchase_id.penalty_id:
                        res[line.id] = 10.0
        return res     
        
    
    def _get_payments_method(self, cr, uid, context=None):
        account_obj = self.pool.get('account.account')
        payment_ids = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','payment_account_ids')
        
        result = []
        if not payment_ids:
            return result
            
        if payment_ids is None:
            return result
        
        for account in account_obj.browse(cr, uid, payment_ids):
            result.append((account.code, account.name))
            
        return result
        
    def _get_default_payments_method(self, cr, uid, context=None):
        account_obj = self.pool.get('account.account')
        payment_ids = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','payment_account_ids')
        
        preferred =  self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos', 'preferred_method')
        if preferred and preferred != '':
            payment_ids = account_obj.search(cr, uid, [('id','=',preferred)])
            
        result = []
        if not payment_ids:
            return result
            
        if payment_ids is None:
            return result
        
        for account in account_obj.browse(cr, uid, payment_ids):
            return account.code
    
    def _get_product_category_id(self, cr, uid, context=None):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos', 'product_category_id')
		
    def unlink(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
            
        
        pharmacy_pos = self.read(cr, uid, ids, ['state'], context=context)
        unlink_ids = []

        for t in pharmacy_pos:
            if t['state'] not in ('draft'):
                raise openerp.exceptions.Warning(_('You cannot delete a pharmacy pos entry statement which is not draft. '))
            else:
                unlink_ids.append(t['id'])

        osv.osv.unlink(self, cr, uid, unlink_ids, context=context)
        return True
        
    def _get_prescription_type(self, cr, uid, context=None):
        return (
                ('paid','Sales'),
                ('foc','Free Of Charge'),
                ('credit','Credit'),
                ('internal','Internal Use'),
            )
            
    def onchange_discount(self, cr, uid, ids, discount, context=None):
        result = {'value' : {}}
        
        if discount > 0.0:
            result['value'].update({
                'type' : 'discount',
            })
            
        if discount == 0.0:
            result['value'].update({
                'type' : 'paid',
            })
            
        return result
        
    def _get_month(self, cr, uid, ids, fields, arg, context=None):
        res = {}
        data = self.browse(cr, uid, ids)[0]
        if data.payment_date:
            date_str = str(data.payment_date)
            month = int(date_str[5:-3])
        
            res[data.id] = month
        return res
    
    def _get_year(self, cr, uid, ids, fields, arg, context=None):
        res = {}
        data = self.browse(cr, uid, ids)[0]
        if data.payment_date:
            date_str = str(data.payment_date)
            year = int(date_str[:4])
        
            res[data.id] = year
        return res
        
    def _get_day(self, cr, uid, ids, fields, arg, context=None):
        res = {}
        data = self.browse(cr, uid, ids)[0]
        if data.payment_date:
            date_str = str(data.payment_date)
            day = int(date_str[-2:])
        
            res[data.id] = day
        return res
    
    
    _columns = {
        'state' : fields.selection([('draft','Draft'), ('credit','Credit'),('close','Closed')], 'Status', readonly=True),
        'patient_id' : fields.many2one('res.partner', 'Partner', required=False),
        #'patient_id' : fields.many2one('isf.oh.patient','Patient', required=False),
        'name' : fields.char('Document Number', size=64, required=True),
        'type' : fields.selection(_get_prescription_type,'Type',required=True),
        'date' : fields.date('Date', required=True),
        'payment_date' : fields.date('Payment Date', required=False),
        'description' : fields.char('Description', size=128, required=False),
        'product_line_ids' : fields.many2many('isf.pharmacy.product.line',string="Product Line", required=True),
        'payment_method' : fields.selection(_get_payments_method,'Payment Method', required=True),
        'total_amount' : fields.function(_calculate_total, type="float", string="Total Amount",store=True),
        'total_free' : fields.function(_calculate_free, type="float", string="Free of Charge",store=True),
        'total_discount' : fields.function(_calculate_discount,"Discount",type="float",string="Discount",store=True),
        'total_paid' : fields.integer("Paid"),
        'account_move_1' : fields.many2one('account.move','Move', required=False),
        'account_move_2' : fields.many2one('account.move','Move', required=False),
        'stock_move_ids' : fields.char('Stock Move', size=32, required=False),
        'kit_id' : fields.many2one('isf.pharmacy.kit','Kit', required=False),
        'year' : fields.function(_get_year,type="integer",string="Year",store=True),
        'month' : fields.function(_get_month,type="integer",string="Month",store=True),
        'day' : fields.function(_get_day,type="integer",string="Day",store=True),
        'invoice_id' : fields.many2one('account.invoice','Invoice Id'),
        'stock_move_ref' : fields.char('Stock Move Ref'),
        'sequence_id': fields.char('PoS ID', size=32,required=False),
        'product_category_id' : fields.integer('Product category'),
    }
    
    _order = "name,date desc"
    
    _defaults = {
        'type' : 'paid',
        'state' : 'draft',
        'date' : fields.date.context_today,
        'payment_date' : fields.date.context_today,
        'payment_method' : _get_default_payments_method,
        'account_move_1' : False,
        'account_move_2' : False,
        'product_category_id' : _get_product_category_id
    }
    
    #_sql_constraints = [('gr_unique_pharmacy_pos', 'unique(name)', 'G.R. number already exists')]
    
    def create(self, cr, uid, vals, context=None):
        obj_sequence = self.pool.get('ir.sequence')
        obj_period = self.pool.get('account.period')
        seq_ids = obj_sequence.search(cr, uid, [('name','=','PH')])
        if len(seq_ids) == 0:
            self.create_sequence(cr, uid, vals)
            seq_ids = obj_sequence.search(cr, uid, [('name','=','PH')])
        
        period_ids = obj_period.find(cr, uid, vals['date'], context=context)
        period = obj_period.browse(cr, uid, period_ids)[0]
        ctx = context.copy()
        ctx.update({
            'fiscalyear_id': period.fiscalyear_id.id,
        })
        new_seq = obj_sequence.next_by_id(cr, uid, seq_ids[0],ctx)
        
        vals['sequence_id'] = new_seq
        return super(isf_pharmacy_pos, self).create(cr, uid, vals, context)
    
    def create_sequence(self, cr, uid, vals, context=None):
        """ Create new no_gap entry sequence for every new Joural
        """
        # in account.journal code is actually the prefix of the sequence
        # whereas ir.sequence code is a key to lookup global sequences.
        prefix = '1'
        
        seq = {
            'name': 'PH',
            'implementation':'no_gap',
            'prefix': "PH/%(year)s/",
            'padding': 6,
            'number_increment': 1
        }
        return self.pool.get('ir.sequence').create(cr, uid, seq)
    
    #
    # Get value from config
    #
    
    def _get_journal_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','journal_id')
        
    def _get_stock_journal_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','stock_journal_id')
        
    def _get_stock_account_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','stock_account_id')
        
    def _get_analytic_account_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','analytic_account_id')
        
    def _get_stock_analytic_account_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','stock_analytic_account_id')
        
    def _get_income_account_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','income_account_id')
        
    def _get_free_of_charge_account_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','free_of_charge_account_id')
        
    def _get_cost_of_item_sold_account_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','cost_of_item_sold_account_id')
        
    def _get_post_on_confirm(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','post_on_confirm')
    
    def _get_stock_location_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','stock_location_id')
        
    def _get_output_location_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','output_location_id')
        
    def _get_enable_stock(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','enable_stock')
    
    def _get_enable_lot_management(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','enable_lot_management')
    
    def _get_payment_method(self, cr, uid, account_code):
        account_obj = self.pool.get('account.account')
        account_ids = account_obj.search(cr, uid, [('code','=',account_code)])
        account = account_obj.browse(cr, uid, account_ids)[0]
        return account.id
        
    def _get_internal_account_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','internal_account_id')
                    
    def _generate_cost_of_items_moves(self, cr, uid, move_line_pool,name, cost, move_cost_id, cost_of_item_sold_account_id, 
        stock_account_id, analytic_account_id, stock_analytic_account_id, context=None):
        move_line_cost1 = {
                'analytic_account_id': analytic_account_id, 
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : name,
                'name': 'Cost Of Items',
                'currency_id': False,
                'credit': 0.0,
                'debit': cost,
                'date_maturity' : False,
                'amount_currency': False,
                'partner_id': False,
                'move_id': move_cost_id,
                'account_id': cost_of_item_sold_account_id,
                'state' : 'valid',
            }
        
        move_line_cost2 = {
                'analytic_account_id': False, 
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : name,
                'name': 'Cost Of Items',
                'currency_id': False,
                'credit': cost,
                'debit': 0.0,
                'date_maturity' : False,
                'amount_currency': False,
                'partner_id': False,
                'move_id': move_cost_id,
                'account_id': stock_account_id,
                'state' : 'valid',
            }
    
        res1 = move_line_pool.create(cr, uid, move_line_cost1,context=context,check=False)
        res2 = move_line_pool.create(cr, uid, move_line_cost2,context=context,check=False)
        
        return res1 and res2
        
    def _generate_stock_move(self, cr, uid, name, date, product, lot_id, qty, stock_location_id, output_location_id, origin,uom_id,context=False):
        stock_obj = self.pool.get('stock.move')
        stock_ids = []
    
        move_ids = stock_obj.create(cr, uid, {
            'name': name,
            'date': date,
            'date_expected' : date,
            'product_uom':  uom_id.id or product_id.uom_id.id,
            'product_uos':  uom_id.id or product_id.uom_id.id,
            'picking_id': False,
            'prodlot_id' : lot_id,
            'product_id': product.id,
            'product_uos_qty': qty / uom_id.factor,
            'product_qty': qty / uom_id.factor,
            'tracking_id': False,
            'state': 'draft',
            'location_id': stock_location_id,
            'location_dest_id': output_location_id,
            'note' : origin,
        }, context=context)
        
        stock_ids.append(move_ids)
        stock_obj.write(cr, uid, move_ids, {'state':'done'}, context=context)
        
    
    def _get_last_cost_price(self, cr, uid, product_id):
        lot_obj = self.pool.get('stock.production.lot')
        lot_ids = lot_obj.search(cr, uid, [('product_id','=',product_id)],order="date")
        if lot_ids:
            lot = lot_obj.browse(cr, uid, lot_ids)[0]
            return lot.cost_price
        
        return 0.0
        
    def _get_discount_account_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','discount_account_id')
    
    
    def get_preferred_method(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','preferred_method')
        
    def get_error_message_on_qty(self,cr, uid, move_ids,product_id,location_id):
        qty = 0.0
        for data in self.browse(cr, uid, move_ids):
            for line in data.product_line_ids:
                if line.product.id == product_id:
                    qty += line.amount
        
        now = datetime.datetime.now()
        str_time = now.strftime("%Y-%m-%d %H:%m:00")         
        stock_prod_lot_obj = self.pool.get('stock.production.lot')
        stock_prod_lot_ids = stock_prod_lot_obj.search(cr, uid, [('product_id','=',product_id),('life_date','>=',str_time)], order='life_date,date')
        
        ctx = {}
        ctx.update({
            'location_id' : location_id,
        })
        
        lot_qty = 0.0
        for lot in stock_prod_lot_obj.browse(cr, uid, stock_prod_lot_ids,context=ctx):
            lot_qty += lot.stock_available
        
        result = {'used_qty' : qty, 'lot_qty' : lot_qty}
        return result
        
    def _get_percentage_cost_price(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.cims.config','percentage_cost_price')
    
    
    def _get_cost_price(self, lot, product,cost_percentage):
        if lot['cost_price'] != product.list_price:
            cost_price = lot['cost_price']
        elif product.standard_price != product.list_price and product.standard_price > 0:
            cost_price = product.standard_price
        else:
            cost_price = product.list_price * (cost_percentage/100)
        
        return cost_price
    #
    # Confirm Method
    #   
    
    def _confirm(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
            
        product_obj = self.pool.get('product.product')
        move_line_pool = self.pool.get('account.move.line')		
        move_pool = self.pool.get('account.move')
        product_line_obj = self.pool.get('isf.pharmacy.product.line')
        isf_lib = self.pool.get('isf.lib.utils')
        
            
        
        #
        # If the user try to confirm a closed prescription
        # the function exit with False
        #
        
        period_id = self._get_actual_period_id(cr, uid, context=context)
        journal_id = self._get_journal_id(cr, uid)
        stock_journal_id = self._get_stock_journal_id(cr, uid)
        stock_account_id = self._get_stock_account_id(cr, uid)
        analytic_account_id = self._get_analytic_account_id(cr, uid)
        stock_analytic_account_id = self._get_stock_analytic_account_id(cr, uid)
        free_of_charge_account_id = self._get_free_of_charge_account_id(cr, uid)
        cost_of_item_sold_account_id = self._get_cost_of_item_sold_account_id(cr, uid)
        discount_account_id = self._get_discount_account_id(cr, uid)
        stock_location_id = self._get_stock_location_id(cr, uid)
        output_location_id = self._get_output_location_id(cr, uid)
        stock_enabled = self._get_enable_stock(cr, uid)
        pos = self._get_post_on_confirm(cr, uid)
        enable_lot = self._get_enable_lot_management(cr, uid)
        income_account_id = self._get_income_account_id(cr, uid)
        origin = False
        internal_account_id = self._get_internal_account_id(cr, uid)
        payment_method_id = self.get_preferred_method(cr, uid)
        cost_percentage = self._get_percentage_cost_price(cr, uid)
        
        if not cost_percentage or cost_percentage is None or cost_percentage == 0:
            cost_percentage = 100.0
        
        # Check product availability
        unavailable = False
        lot_id = False
        
        
        move_paid_ids = []
        move_foc_ids = []
        move_credit_ids = []
        move_internal_ids = []
        stock_move_dict = {}
        stock_move_lot_dict = {}
        
        total_income = 0.0
        total_discount = 0.0
        total_foc = 0.0
        total_cost_of_items = 0.0
        total_cost_internal = 0.0
        have_paid = False
        have_foc = False
        have_credit = False
        have_internal = False
        
        for data in self.browse(cr, uid, ids):
            if data.type == 'paid' and data.state != 'close':
                move_paid_ids.append(data.id)
            elif data.type == 'foc' and data.state != 'close':
                move_foc_ids.append(data.id)
            elif data.type == 'credit' and data.state != 'close':
                move_credit_ids.append(data.id)
            elif data.type == 'internal' and data.state != 'close':
                move_internal_ids.append(data.id)
        
        now = datetime.datetime.now()
        str_time = now.strftime("%H:%m:00")
        move_date = data.date + " "+str_time
        
        if _debug:
            _logger.debug('Stock Location : %s',  stock_location_id)               
        
        # Paid Prescriptions
        for data in self.browse(cr, uid, move_paid_ids):
            have_paid = True
            for line in data.product_line_ids:
                product = line.product
                uom_id = product.uom_id
                lot_ids = isf_lib.get_lot_id(cr, uid, product.id, line.amount, stock_location_id, data.date, True)
                if not lot_ids:
                    result = self.get_error_message_on_qty(cr, uid, move_paid_ids, product.id, stock_location_id)
                    str_message = "Product : "+product.name_template+" out of stock. Check before proceed. Quantity available on stock "+str(result['lot_qty'])+" | Used "+str(result['used_qty'])+". "
                    raise osv.except_osv(_('Error!'), _(str_message))
                    return False
                for lot in lot_ids:
                    cost_price = self._get_cost_price(lot, product,cost_percentage)
                    total_cost_of_items += lot['qty'] * cost_price  
                    stock_ids = self._generate_stock_move(cr, uid, data.name, move_date, product, lot['id'], lot['qty'], stock_location_id, output_location_id, data.sequence_id,product.uom_id,context=context)    
                    self.write(cr, uid, ids, {'stock_move_ref' : data.name}, context=context)
               
                product_line_obj.write(cr, uid, line.id, {'state' : 'close'}, context=context)  
                total_income += line.price * line.amount
            self.write(cr, uid, data.id, {'payment_date':data.date},context=context) 
            total_discount += data.total_discount
        
        # FoC Prescription
        for data in self.browse(cr, uid, move_foc_ids):
            have_foc = True
            for line in data.product_line_ids:
                product = line.product
                uom_id = product.uom_id
                lot_ids = isf_lib.get_lot_id(cr, uid, product.id, line.amount, stock_location_id, data.date, True)
                if not lot_ids:
                    result = self.get_error_message_on_qty(cr, uid, move_paid_ids, product.id, stock_location_id)
                    str_message = "Product : "+product.name_template+" out of stock. Check before proceed. Quantity available on stock "+str(result['lot_qty'])+" | Used "+str(result['used_qty'])+". "
                    raise osv.except_osv(_('Error!'), _(str_message))
                    return False
                for lot in lot_ids:
                    cost_price = self._get_cost_price(lot, product,cost_percentage)
                    total_foc += cost_price * lot['qty']
                    stock_ids = self._generate_stock_move(cr, uid, data.name, move_date, product, lot['id'], lot['qty'], stock_location_id, output_location_id, data.sequence_id,product.uom_id,context=context)    
                    self.write(cr, uid, ids, {'stock_move_ref' : data.name}, context=context)
                product_line_obj.write(cr, uid, line.id, {'state' : 'close'}, context=context)
            self.write(cr, uid, data.id, {'payment_date':data.date},context=context) 
                    
        # Internal Prescription
        for data in self.browse(cr, uid, move_internal_ids):
            have_internal = True
            for line in data.product_line_ids:
                product = line.product
                uom_id = product.uom_id
                lot_ids = isf_lib.get_lot_id(cr, uid, product.id, line.amount, stock_location_id, data.date, True)
                if not lot_ids:
                    result = self.get_error_message_on_qty(cr, uid, move_paid_ids, product.id, stock_location_id)
                    str_message = "Product : "+product.name_template+" out of stock. Check before proceed. Quantity available on stock "+str(result['lot_qty'])+" | Used "+str(result['used_qty'])+". "
                    raise osv.except_osv(_('Error!'), _(str_message))
                    return False
                for lot in lot_ids:
                    cost_price = self._get_cost_price(lot, product,cost_percentage)
                    total_cost_internal += lot['qty'] * cost_price
                    stock_ids = self._generate_stock_move(cr, uid, data.name, move_date, product, lot['id'], lot['qty'], stock_location_id, output_location_id,data.sequence_id,product.uom_id,context=context)
                    self.write(cr, uid, ids, {'stock_move_ref' : data.name}, context=context)    
                
                product_line_obj.write(cr, uid, line.id, {'state' : 'close'}, context=context)
                total_income += line.price * line.amount
            total_discount += data.total_discount
            self.write(cr, uid, data.id, {'payment_date':data.date},context=context) 
            
        # Credit Prescriptions
        for data in self.browse(cr, uid, move_credit_ids):
            have_credit = True
            for line in data.product_line_ids:
                product = line.product
                uom_id = product.uom_id
                lot_ids = isf_lib.get_lot_id(cr, uid, product.id, line.amount, stock_location_id, data.date, True)
                if not lot_ids:
                    result = self.get_error_message_on_qty(cr, uid, move_paid_ids, product.id, stock_location_id)
                    str_message = "Product : "+product.name_template+" out of stock. Check before proceed. Quantity available on stock "+str(result['lot_qty'])+" | Used "+str(result['used_qty'])+". "
                    raise osv.except_osv(_('Error!'), _(str_message))
                    return False
                for lot in lot_ids:
                    cost_price = self._get_cost_price(lot, product,cost_percentage)
                    total_cost_of_items += lot['qty'] * cost_price
                    stock_ids = self._generate_stock_move(cr, uid, data.name, move_date, product, lot['id'], lot['qty'], stock_location_id, output_location_id,data.sequence_id,product.uom_id,context=context)
                    self.write(cr, uid, ids, {'stock_move_ref' : data.name}, context=context)
                    
                total_income += line.price * line.amount
            total_discount += data.total_discount
                
        # Paid Move
        if have_paid or have_internal:
            move = move_pool.account_move_prepare(cr, uid, journal_id, data.date, data.name, context=context)	
            move_id = move_pool.create(cr, uid, move, context=context)
            self.write(cr, uid, ids, {'account_move_1':move_id},context=context)
        #
        # Income on sell the item
        #
    
        if total_income > 0.0 and not have_credit:
            move_line_credit = {
                'analytic_account_id': analytic_account_id, 
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : data.name,
                'name':  'Drugs',
                'currency_id': False,
                'credit': total_income ,
                'debit': 0.0,
                'date_maturity' : False,
                'amount_currency': False,
                'partner_id': False,
                'move_id': move_id,
                'account_id': income_account_id,
                'state' : 'valid',
            }
    
            result = move_line_pool.create(cr, uid, move_line_credit,context=context,check=False)
        
            #
            # Money paid
            #
            move_line_paid = {
                'analytic_account_id': False, 
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : data.name,
                'name': 'Drugs' or '/',
                'currency_id': False,
                'credit': 0.0,
                'debit': total_income - total_discount,
                'date_maturity' : False,
                'amount_currency': False,
                'partner_id': False,
                'move_id': move_id,
                'account_id': payment_method_id,
                'state' : 'valid' }
            result = move_line_pool.create(cr, uid, move_line_paid,context=context,check=False)
                
            if total_discount > 0:
                move_line_discount = {
                    'analytic_account_id': analytic_account_id, 
                    'tax_code_id': False, 
                    'tax_amount': 0,
                    'ref' : data.name,
                    'name': 'Discount',
                    'currency_id': False,
                    'credit': 0.0,
                    'debit': total_discount,
                    'date_maturity' : False,
                    'amount_currency': False,
                    'partner_id': False,
                    'move_id': move_id,
                    'account_id': discount_account_id,
                    'state' : 'valid' }
                result = move_line_pool.create(cr, uid, move_line_discount,context=context,check=False)
        
        if stock_enabled:
            if total_foc > 0.0 or total_cost_of_items > 0.0 or total_cost_internal > 0.0:
                move_cost = move_pool.account_move_prepare(cr, uid, stock_journal_id, data.date, data.name, context=context)	
                move_cost_id = move_pool.create(cr, uid, move_cost, context=context)
                self.write(cr, uid, ids, {'account_move_2':move_cost_id},context=context)
            
                if total_foc > 0.0:    
                    self.k(cr, uid, move_line_pool, data.name, total_foc, 
                        move_cost_id, free_of_charge_account_id, stock_account_id, analytic_account_id, analytic_account_id, context=context)
                if total_cost_of_items > 0.0:
                    self._generate_cost_of_items_moves(cr, uid, move_line_pool, data.name, total_cost_of_items, 
                        move_cost_id, cost_of_item_sold_account_id, stock_account_id, analytic_account_id, analytic_account_id, context=context)
                if total_cost_internal > 0.0:
                    self._generate_cost_of_items_moves(cr, uid, move_line_pool, data.name, total_cost_internal, 
                        move_cost_id, internal_account_id, stock_account_id, analytic_account_id, analytic_account_id, context=context)
        
        self._manage_credit(cr, uid, move_credit_ids, context=context)
        
        self.write(cr, uid, move_internal_ids, {'state' : 'close'}, context=context)
        self.write(cr, uid, move_paid_ids, {'state' : 'close'}, context=context)
        self.write(cr, uid, move_foc_ids, {'state' : 'close'}, context=context)
        
        
        return True
        
    
    def register_payment(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        inv_obj = self.pool.get('account.invoice')
        inv = inv_obj.browse(cr, uid, data.invoice_id.id, context=context)
        
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'account_voucher', 'view_vendor_receipt_dialog_form')
        return {
            'name':_("Pay Invoice"),
            'view_mode': 'form',
            'view_id': view_id,
            'view_type': 'form',
            'res_model': 'account.voucher',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
            'context': {
                'payment_expected_currency': inv.currency_id.id,
                'default_partner_id': self.pool.get('res.partner')._find_accounting_partner(inv.partner_id).id,
                'default_amount': inv.type in ('out_refund', 'in_refund') and -inv.residual or inv.residual,
                'default_reference': inv.name,
                'close_after_process': True,
                'invoice_type': inv.type,
                'invoice_id': inv.id,
                'default_type': inv.type in ('out_invoice','out_refund') and 'receipt' or 'payment',
                'type': inv.type in ('out_invoice','out_refund') and 'receipt' or 'payment'
            }
        }
        
    
    def get_discount_product_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','discount_product_id')

    def _get_credit_journal_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','credit_journal_id')
        
    def _manage_credit(self, cr, uid, ids, context=None):
        inv_obj = self.pool.get('account.invoice')
        inv_line_obj = self.pool.get('account.invoice.line')
        analytic_account_id = self._get_analytic_account_id(cr, uid)    
        free_of_charge_account_id = self._get_free_of_charge_account_id(cr, uid)
        income_account_id =self._get_income_account_id(cr, uid)
        journal_id = self._get_credit_journal_id(cr, uid)
        for data in self.browse(cr, uid, ids):
            self.write(cr, uid, data.id, {'payment_date' : False}, context=context)
            vals = {
                'type' : 'out_invoice',
                'partner_id' : data.patient_id.id,
                'account_id': data.patient_id.property_account_receivable.id,
                'date_due' : data.date,
                'date_invoice' : data.date,
                'journal_id' : journal_id,

            }
            inv_id = inv_obj.create(cr, uid, vals, context=context)
        
            for line in data.product_line_ids:
                line_vals = {
                    'name' : line.product.name_template,
                    'invoice_id' : inv_id,
                    'product_id': line.product.id,
                    'account_id' : income_account_id,
                    'quantity' : line.amount,
                    'price_unit' : line.price,
                    'account_analytic_id' : analytic_account_id,
                }
                line_id = inv_line_obj.create(cr, uid, line_vals, context=context)
                
            if data.total_discount > 0:
                prod_o = self.pool.get('product.product')
                discount_id = self.get_discount_product_id(cr, uid)
                product = prod_o.browse(cr, uid, [discount_id])[0]
                account_id = self._get_discount_account_id(cr, uid)
                line_vals = {
                    'name' : 'Discount',
                    'invoice_id' : inv_id,
                    'product_id': discount_id,
                    'account_id' : account_id,
                    'quantity' : 1.0,
                    'price_unit' : -1.0 * data.total_discount,
                    'account_analytic_id' : analytic_account_id,
                }
                line_id = inv_line_obj.create(cr, uid, line_vals, context=context)
                    
        
            self.write(cr, uid, [data.id], {'invoice_id' : inv_id, 'state' : 'credit' }, context=context)
        
            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(uid, 'account.invoice', inv_id, 'invoice_open', cr)

        
        return True        
    
    def confirm_prescription(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        self._confirm(cr, uid, ids, context=context)
        search_wizard = context.get('search_wizard')
        if search_wizard:
            return self._reopen_window(self._name, data.id)
        return True
        
    def confirm_prescription_and_new(self, cr, uid, ids, context=None):
        self._confirm(cr, uid, ids, context=context)
        self.write(cr, uid, ids, {'state':'close'}, context=context)
        
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_type': 'form',
            'res_model': self._name,
            # save original model in context, because selecting the list of available
            # templates requires a model in context
            'context': {
                'default_model': 'isf.pharmacy.pos',
            },
        }
        
    def save_prescription_and_new(self, cr, uid, ids, context=None):
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_type': 'form',
            'res_model': self._name,
            # save original model in context, because selecting the list of available
            # templates requires a model in context
            'context': {
                'default_model': 'isf.pharmacy.pos',
            },
        }
    
    def _get_actual_period_id(self, cr, uid, context=None):
        if context is None:
            context = {}
			
        period_pool = self.pool.get('account.period')
        period_ids = period_pool.search(cr, uid, [])
        period_obj = period_pool.browse(cr, uid, period_ids, context=context)
		
        now = datetime.datetime.now()
        now_str = str(now.year) + "-" + str(now.month) + "-" + str(now.day)
		
        period_id = None
        for period in period_obj:
            if now_str <= period.date_stop and now_str >= period.date_start:
                period_id = period.id
				
        return period_id
        
    def button_reset_total(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids, context=context)[0]
        total_amount = 0.0
        total_free = data.total_free
        total_discount = data.total_discount
        for line in data.product_line_ids:
            total_amount = total_amount + line.total_computed 
        
        total_amount = total_amount - total_discount
        self.write(cr, uid, ids, {'total_amount': total_amount}, context=context)
        self.write(cr, uid, ids, {'total_free': total_free}, context=context)
        self.write(cr, uid, ids, {'total_discount': total_discount}, context=context)
   
   
    def _remove_journal_entry(self, cr, uid, ids):
        data = self.browse(cr, uid, ids)[0]
        move_obj = self.pool.get('account.move')
        
        move_ids = []
        if data.account_move_1:
            move_ids.append(data.account_move_1.id)
        
        if data.account_move_2:
            move_ids.append(data.account_move_2.id)
        
        move_obj.button_cancel(cr, uid, move_ids)
        move_obj.unlink( cr, uid, move_ids, context=None, check=False)
            
        return True
    
    def _remove_stock_move(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        move_obj = self.pool.get('stock.move')
        
        stock_move_ids = move_obj.search(cr, uid, [('name','=',data.name)])
        move_obj.write(cr, uid, stock_move_ids, {'state':'cancel'}, context=context)
        
        stock_move_ids = move_obj.search(cr, uid, [('name','=',data.stock_move_ref),('state','=','done')])
        move_obj.write(cr, uid, stock_move_ids, {'state':'cancel'}, context=context)
        
        return True
        
    def _cancel_invoice(self, cr, uid, ids,context=None):
        data = self.browse(cr, uid, ids)[0]
        inv_obj = self.pool.get('account.invoice')
        #wf_service = netsvc.LocalService("workflow")
        #wf_service.trg_delete(uid, 'account.invoice', data.invoice_id.id, cr)
        if data.invoice_id:
            inv_obj.action_cancel(cr, uid, [data.invoice_id.id], context=context)
        return True

    
    def set_to_draft(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        
        if _debug:
            _logger.debug('CONTEXT : %s', context)
        
        self.write(cr, uid, [data.id], {'state':'draft'}, context=context)
        
        if data.type == 'credit':
            if self._cancel_invoice(cr, uid, ids, context):
                self.write(cr, uid, ids, {'invoice_id':False}, context=context)
           
        if self._remove_journal_entry(cr, uid, ids):
            self.write(cr, uid, ids, {'account_move_1':False,'account_move_2':False}, context=context)
            
        if self._remove_stock_move(cr, uid, ids):
            self.write(cr, uid, ids, {'stock_move_ids':False}, context=context)
        
        line_o = self.pool.get('isf.pharmacy.product.line')
        for line in data.product_line_ids:
            line_o.write(cr, uid, line.id, {'state' : 'draft'}, context=context)
        
        self.write(cr, uid, ids, {'payment_date':False}, context=context)
        search_wizard = context.get('search_wizard')
        if search_wizard:
            return self._reopen_window(self._name, data.id)
            
        return True    
        
    def _reopen_window(self, name, data_id):
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_type': 'form',
            'res_id': data_id,
            'res_model': name,
            'target': 'new',
            # save original model in context, because selecting the list of available
            # templates requires a model in context
            'context': {
                'default_model': 'isf.pharmacy.pos',
                'search_wizard' : 1,
                'ref' : False,
            },}
        
    def onchange_kit(self,cr, uid, ids, kit_id, p_type, context=None):
        if context is None:
            context = {}
            
        kit_obj = self.pool.get('isf.pharmacy.kit')
        kit_ids = kit_obj.search(cr, uid, [('id','=',kit_id)])
        kit = kit_obj.browse(cr, uid, kit_ids)[0]
        
        line_obj = self.pool.get('isf.pharmacy.product.line')
        mult = self._get_mult(p_type)
        
        result = {'value': {} }
        line_ids = []
        total_amount = 0.0
        for line in kit.product_line_ids:
        
            total_computed = line.product.list_price * line.qty * mult
            total_amount += total_computed
        
            line_id = line_obj.create(cr, uid, {
                'product' : line.product.id,
                'amount' : line.qty,
                'price' : line.product.list_price,
                'total_computed' : total_computed,
            }, context=context)
            
            line_ids.append(line_id)
        
        result['value'].update({
            'product_line_ids' : line_ids,
            'total_amount' : total_amount,
        })
        
        return result
        
    def onchange_date(self, cr, uid, ids, date, context=None):
        result = {'value':{}}
        
        result['value'].update({
            'payment_date' : date,
        })
        
        return result
    
isf_pharmacy_pos()

class isf_pharmacy_pos_confirm(osv.osv_memory):
    _name = 'isf.pharmacy.pos.confirm'
    
    def multiple_confirm(self, cr, uid, ids, context=None):
        pos_obj = self.pool.get('isf.pharmacy.pos')
        active_ids = context['active_ids']
        
        for pos_id in active_ids: 
            pos_obj.confirm_prescription(cr, uid, [pos_id], context=context)
            
        return True
        
        
    def multiple_confirm2(self, cr, uid, ids, context=None):
        pos_obj = self.pool.get('isf.pharmacy.pos')
        active_ids = context['active_ids']
        
        for pos in pos_obj.browse(cr, uid, active_ids): 
            if pos.type == 'paid':
                pos_obj.write(cr, uid, pos.id, {'state':'close'}, context=None)
            if pos.type == 'credit' and pos.state == 'draft':
                pos_obj.confirm_prescription(cr, uid, [pos.id], context=context)
            
        return True
        
    def cancel(self, cr, uid, ids, context=None):
        return {'type': 'ir.actions.act_window_close'}
        
isf_pharmacy_pos_confirm()

#lass isf_pharmacy_pos_draft(osv.osv_memory):
#    _name = 'isf.pharmacy.pos.draft'
#
#    def multiple_draft(self, cr, uid, ids, context=None):
#        pos_obj = self.pool.get('isf.pharmacy.pos')
#        active_ids = context['active_ids']
#        
#        for pos_id in active_ids: 
#            pos_obj.set_to_draft(cr, uid, [pos_id], context=context) 
#        return True
#
#isf_pharmacy_pos_draft()

class isf_pharmacy_pos_search_wizard(osv.TransientModel):
    _name = 'isf.pharmacy.pos.search.wizard'
    
    _rec_name = 'product_id'
    
    _columns = {
        'product_id' : fields.many2one('product.product','Product'),
        'date_start' : fields.date('Date Start'),
        'date_stop' : fields.date('Date Stop'),
        'prescription_ids' : fields.many2many('isf.pharmacy.pos','pos_search_id','pos_id','isf_pharmacy_pos_search_rel', required=True,readonly=True)
    }
    
    _defaults = {
        'date_start' : fields.date.context_today,
        'date_stop' : fields.date.context_today,
    }
    
    def search_prescriptions(self, cr, uid, ids, product_id, date_start, date_stop, context=None):
        if context is None:
            context = {}
            
        result = {'value' : {}}
    
        if not product_id:
            return False
            
        str_query = "SELECT DISTINCT(isf_pharmacy_pos_id) FROM isf_pharmacy_pos_isf_pharmacy_product_line_rel rel,isf_pharmacy_pos pos \
            where pos.date >= '"+date_start+"' and pos.date <= '"+date_stop+"' and pos.id = rel.isf_pharmacy_pos_id and rel.isf_pharmacy_product_line_id in \
            (select id from isf_pharmacy_product_line where product="+str(product_id)+")"
        
        if _debug:
            _logger.debug('Query : %s',str_query)
            
        cr.execute(str_query)
        res_list = cr.fetchall()
        
        line_ids = []
        for res in res_list:
            if _debug:
                _logger.debug('RES : %s',res)
            line_ids.append(res[0])
                
        result['value'].update({
            'prescription_ids' : line_ids,
        })
                
        return result
