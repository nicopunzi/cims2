from openerp.osv import fields, osv
from tools.translate import _
import logging
import datetime
import openerp

_logger = logging.getLogger(__name__)
_debug=False

class isf_hr_payroll_advances(osv.Model):
    _name = 'isf.hr.payroll.advances'
    
    def _get_default_journal_id(self, cr, uid, context=None):
       return self.pool.get('ir.values').get_default(cr, uid, 'isf.hr.payroll','advances_journal_id')
        
    def _get_cash_bank_accounts(self, cr, uid, context=None):
        account_obj = self.pool.get('account.account')
        account_ids = self.pool.get('ir.values').get_default(cr, uid, 'isf.hr.payroll','cash_bank_account_ids')
        
        result = []
        if not account_ids:
            return result
            
        if account_ids is None:
            return result
        
        for account in account_obj.browse(cr, uid, account_ids):
            result.append((account.code, account.name))
            
        return result
    
        
    _columns = {
        'state' : fields.selection([('draft','Draft'),('done','Confirmed')],'state'),
        'journal_id' : fields.many2one('account.journal','Journal', required=False),
        'ref' : fields.char('Reference', size=64,required=True),
        'name' : fields.char('Description', size=64, required=True),
        'date' : fields.date('Date', required=True),
        'currency_view' : fields.many2one('res.currency', 'Currency View'),
        'currency' : fields.many2one('res.currency', 'Currency', required=False),
        'currency_amount' : fields.float('Currency Amount',digits=(12,4)),
        'company_amount' : fields.float('Company Amount',digits=(12,4)),
        'cash_account' : fields.selection(_get_cash_bank_accounts,'Cash\Bank account', required=True),
        'help' : fields.text('Help', size=512),
        'employee_id' : fields.many2one('hr.employee','Employee',required=True),
        'analytic_account_id' : fields.many2one('account.analytic.account','Analytic Account',required=True),
        'move_id' : fields.many2one('account.move','Move Id'),
        'period_id' : fields.many2one('account.period','Period'),
    }
    
    _defaults = {
        'journal_id' : _get_default_journal_id,
        'date' : fields.date.context_today,
        'state' : 'draft',
        #'help' : _get_help,
    }
    
    def _check_amount(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
            
        if obj.currency_amount <= 0.0 :
            return False
        return True

    _constraints = [
         (_check_amount, 'Amount must be positive ( > 0.0)',['amount']),
    ]
    
    def _get_actual_period_id(self, cr, uid, context=None):
        if context is None:
            context = {}
			
        period_pool = self.pool.get('account.period')
        period_ids = period_pool.search(cr, uid, [])
        period_obj = period_pool.browse(cr, uid, period_ids, context=context)
		
        now = datetime.datetime.now()
        now_str = str(now.year) + "-" + str(now.month) + "-" + str(now.day)
		
        period_id = None
        for period in period_obj:
            if _debug:
                if now_str <= period.date_stop and now_str >= period.date_start:
                    period_id = period.id
                    if _debug:
                        _logger.debug('Now : %s Start : %s , Stop : %s',now_str, period.date_start, period.date_stop)
				
        return period_id

    def _get_company_currency_id(self, cr, uid, context=None):
        users = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        company_id = users.company_id.id
        currency_id = users.company_id.currency_id
		
        return currency_id.id
	
    def _get_analytic_journal_by_name(self, cr, uid, name):
        analytic_journal_pool = self.pool.get('account.analytic.journal')
        analytic_journal_ids = analytic_journal_pool.search(cr, uid, [('name','=',name)])
        return analytic_journal_ids[0] if len(analytic_journal_ids) else None
        
    def get_default_advances_account_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.hr.payroll','advances_account_id')
    
    def set_to_draft(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
            
        wizard = self.browse(cr, uid, ids)[0]
        move_obj = self.pool.get('account.move')
        
        move_ids = []
        if wizard.move_id:
            move_ids.append(wizard.move_id.id)
            
        move_obj.button_cancel(cr, uid, move_ids)
        move_obj.unlink( cr, uid, move_ids, context=None, check=False)
        
        self.write(cr, uid, ids, {'state':'draft'}, context=context)
        
        return True
    
    def unlink(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        
        wizard = self.read(cr, uid, ids, ['state'], context=context)
        unlink_ids = []

        for t in wizard:
            if t['state'] not in ('draft'):
                raise openerp.exceptions.Warning(_('You cannot delete an entry which is not draft. '))
            else:
                unlink_ids.append(t['id'])

        osv.osv.unlink(self, cr, uid, unlink_ids, context=context)
        return True
    
    def confirm(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        data = self.browse(cr, uid, ids, context=context)[0]

        if not data.employee_id.address_id:
            if not data.employee_id.address_home_id:
                raise openerp.exceptions.Warning(_('Define home address (accounting) for employee to registare Advances On Payroll '))
                return False

        if data.employee_id.address_home_id:
            partner_id = data.employee_id.address_home_id.id
            employee_account_id = data.employee_id.address_home_id.property_account_payable
        elif data.employee_id.address_id:
            partner_id = data.employee_id.address_id.id
            employee_account_id = data.employee_id.address_id.property_account_payable

        journal_id = self._get_default_journal_id(cr, uid, context=context)
        actual_period_id = self._get_actual_period_id(cr, uid, context=context)
        expense_account_id = self.get_default_advances_account_id(cr, uid)

        move_line_pool = self.pool.get('account.move.line')		
        move_pool = self.pool.get('account.move')
        account_pool = self.pool.get('account.account')
        account_ids = account_pool.search(cr, uid, [('code','=',data.cash_account)])
        account_id = account_pool.browse(cr, uid, account_ids)[0]
		
        move = move_pool.account_move_prepare(cr, uid, journal_id, data.date, ref=data.ref, context=context)	
	
        if _debug:
            _logger.debug('Context : %s', context)
            _logger.debug('Amount_currency : %f', data.currency_amount)
            _logger.debug('account_move_prepare : %s', move)

        move_id = move_pool.create(cr, uid, move, context=context)
        self.write(cr, uid, ids, {'move_id' : move_id}, context=context)
		
        if _debug:
            _logger.debug('move_id : %s', move_id)

		
        company_currency_id = self._get_company_currency_id(cr, uid, context=context)
        currency_id = False
        amount_currency = False
        amount_currency_credit = False
        amount_currency_debit = False
        amount = data.company_amount
		
        
        if data.currency.id != company_currency_id:
            currency_pool = self.pool.get('res.currency')
            if _debug:
                _logger.debug('Setting second currency value')
            amount = currency_pool.compute(cr, uid, data.currency.id, company_currency_id, data.currency_amount, context=context)
            amount_currency_credit = -1 * data.currency_amount
            amount_currency_debit = data.currency_amount
            amount_currency = data.currency_amount
            currency_id = data.currency.id
				
        move_line = {
            'analytic_account_id': False, 
            'tax_code_id': False, 
            'tax_amount': 0,
            'ref' : data.ref,
            'name': data.name or '/',
            'currency_id': currency_id,
            'credit': 0.0,
            'debit': amount,
            'date_maturity' : False,
            'amount_currency': amount_currency_debit,
            'move_id': move_id,
            'account_id': expense_account_id,
            'partner_id' : partner_id,
            'state' : 'valid'
        }
		
        if _debug:
            _logger.debug('move_line : %s',move_line)
		
        result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
		
        if _debug:
            _logger.debug('Result : %s', result)
            _logger.debug('Result : %s', data.cash_account)
            
        account_o = self.pool.get('account.account')
        account_ids = account_o.search(cr, uid, [('code','=',data.cash_account)])
        account = account_o.browse(cr, uid, account_ids)[0]
			
        if _debug:
            _logger.debug('Account : %s', account)
        
        
        move_line = {
            'analytic_account_id': False, 
            'tax_code_id': False, 
            'tax_amount': 0,
            'ref' : data.ref,
            'name': data.name or '/',
            'currency_id': currency_id,
            'credit': amount,
            'debit': 0.0,
            'date_maturity' : False,
            'amount_currency': amount_currency_credit,
            'partner_id': False,
            'move_id': move_id,
            'account_id': account.id,
            'partner_id' : partner_id,
            'state' : 'valid'
        }
	
        result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
		
        if _debug:
            _logger.debug('Result : %s', result)
            
        self.write(cr, uid, ids, {'state' : 'done'})
            
    def onchange_amount(self, cr, uid, ids,  currency,currency_amount,context=None):
        if context is None:
            context = {}
        
        result = {'value':{} }
        currency_pool = self.pool.get('res.currency')
        company_currency = self._get_company_currency_id(cr, uid, context=context)
        company_amount = currency_pool.compute(cr, uid, currency, company_currency, currency_amount, context=context)
        
        
        result['value'].update({
            'company_amount' : company_amount,
        })
        
        return result
        
    def onchange_currency(self, cr, uid, ids, currency, context=None):
        result = {'value':{} }
        result['value'].update({
            'currency_amount' : 0.0,
            'company_amount' : 0.0,
        })
        
        return result
        
        
    def _create_default_journal(self, cr, uid, ids=None, context=None):
        if context is None:
            context = {}
            
        journal_pool = self.pool.get('account.journal')
        journal_ids = journal_pool.search(cr, uid, [('code','=','SAPJ')])
        journal_obj = journal_pool.browse(cr, uid, journal_ids, context=context)
        
        Found = False
        for journal in journal_obj:
            if _debug:
                _logger.debug('Found : %d,%s',journal.id, journal.code)
            Found = True
        
        if Found:  
            _logger.debug('Default journal found') 
        else:
            _logger.debug('Default journal not found : create')
            
            seq_pool = self.pool.get('ir.sequence')
            seq_ids = seq_pool.search(cr, uid, [('name','=','Salary Advances On Payroll Sequence')])
            seq_obj = seq_pool.browse(cr, uid, seq_ids)
            sequence_id = False
            for seq in seq_obj:
                if _debug:
                    _logger.debug('Sequence : %d,%s',seq.id, seq.name)
                sequence_id = seq.id
                break
                
            if not sequence_id:
                seq_vals = {
                    'name' : 'Salary Advances On Payroll Sequence',
                    'prefix' : 'SAP/%(year)s/',
                    'padding' : 4,
                    'implementation' : 'no_gap',
                }
                
                sequence_id = seq_pool.create(cr,uid, seq_vals, context=context) 
            
            journal = {
                'name' : 'Salary Advances On Payroll Journal',
                'code' : 'SAPJ',
                'type' : 'general',
                'sequence_id' : sequence_id,
                'update_posted' : True,
                'analytic_journal_id' : self._get_analytic_journal_by_name(cr, uid, 'General')
            }
            
            journal_pool.create(cr, uid, journal, context=context)
            
        return True
    
    def onchange_employee(self, cr, uid, ids, employee_id, context=None):
        result = {'value':{}}
        result['value'].update({
            'analytic_account_id' : False,
        })
        employee_pool = self.pool.get('hr.employee')
        employee_ids = employee_pool.search(cr,uid,[('id','=',employee_id)])
        employee = employee_pool.browse(cr, uid, employee_ids)[0]
        if employee:
            if _debug:
                _logger.debug('Employee : %s',employee)
                _logger.debug('Address : %s',employee.address_id)
                _logger.debug('Address : %s',employee.address_home_id)
            if not employee.address_id:
                if not employee.address_home_id:
                    raise openerp.exceptions.Warning(_('Define home address (accounting) for employee to registare Advances On Payroll '))
                    return result
                
            if employee.department_id:
                if employee.department_id.analytic_account_id:
                    result['value'].update({
                        'analytic_account_id' : employee.department_id.analytic_account_id.id,
                    })
        
        return result
        
    def onchange_cash_account(self, cr, uid, ids, account_id, context=None):
        if context is None:
            context = {}
        
        if _debug:
            _logger.debug('onchange_cash_account : %s', account_id)
    
        result = {'value':{} }
        account_pool = self.pool.get('account.account')
        account_ids = account_pool.search(cr, uid, [('code','=',account_id)],limit=1)
        account_obj = account_pool.browse(cr, uid, account_ids, context=context)
        
        for account in account_obj:
            currency_id = account.currency_id.id
            
            if not currency_id:
                currency_id = self._get_company_currency_id(cr, uid, context=context)
                
            result['value'].update({
                'currency' : currency_id,
                'currency_view' : currency_id                    
            })
            
            #self.write(cr, uid, ids, {'currency' : currency_id })
            
        return result
        
    def onchange_date(self, cr, uid, ids, date, context=None):
        result = {'value' : {}}
        
        period_o = self.pool.get('account.period')
        period_id = period_o.find(cr, uid, date)
        
        result['value'].update({
            'period_id' : period_id[0],
        })
        
        return result