from openerp.osv import fields, osv
from tools.translate import _
import logging
import datetime
import time
from tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, float_compare
import decimal_precision as dp
import netsvc

_logger = logging.getLogger(__name__)
_debug=False

class hr_employee(osv.Model):
    _inherit = 'hr.employee'
    
    _columns = {
        'unique_code' : fields.char('Code'),
    }
    
    def create(self, cr, uid, vals, context=None):
        obj_sequence = self.pool.get('ir.sequence')
        obj_period = self.pool.get('account.period')
        seq_ids = obj_sequence.search(cr, uid, [('name','=','Employee')])
        date = datetime.datetime.now()
        period_ids = obj_period.find(cr, uid, date, context=context)
        period = obj_period.browse(cr, uid, period_ids)[0]
        ctx = context.copy()
        ctx.update({
            'fiscalyear_id': period.fiscalyear_id.id,
        })
        new_seq = obj_sequence.next_by_id(cr, uid, seq_ids,ctx)
        
        vals['unique_code'] = new_seq
        
        res_id = super(hr_employee, self).create(cr, uid, vals, context=context)  
    
        return res_id

class hr_contract(osv.Model):
    _inherit = 'hr.contract'

    _columns = {
        'contract_attach' : fields.binary('Contract'),
        'insurance_attach' : fields.binary('Insurance'),
        'visa_attach' : fields.binary('Visa'),
    }
    
class hr_department(osv.Model):
    _inherit = 'hr.department'
    
    _columns = {
        'analytic_account_id' : fields.many2one('account.analytic.account','Analytic Account'),
    }

class hr_contract_type(osv.Model):
    _inherit = 'hr.contract.type'
    
    _columns = {
        'incentive' : fields.float('Incentive',required=False),
        'deduction' : fields.float('Deduction',required=False),
    }
    
    _defaults = {
        'incentive' : 0.0,
        'deduction' : 0.0,
    }

class isf_hr_payroll(osv.osv_memory):
    _name = 'isf.hr.payroll'
    _description = 'Multiple payslip'
    
    def _get_period_id(self, cr, uid, date_from, date_to):
        period_pool = self.pool.get('account.period')
        period_ids = period_pool.search(cr, uid, [('date_start','>=',date_from),('date_stop','<=',date_to)])
        
        
        return period_pool.browse(cr, uid, period_ids)[0].id
        
    def scheduled_multiple_payroll(self, cr, uid):
        return True
        
    def _get_analytic_account_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.hr.payroll','analytic_account_id')
    
    def get_default_advances_account_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.hr.payroll','advances_account_id')
    
    def multiple_payroll(self, cr, uid, ids, context=None):
        payslip_obj = self.pool.get('hr.payslip')
        obj_payslip_line = self.pool.get('hr.payslip.line')
        
        active_ids = context['active_ids']
        payslip_ids = payslip_obj.search(cr, uid, [('id','in',active_ids),('state','=','draft')])
        payslip_done_ids = payslip_obj.search(cr, uid, [('id','in',active_ids),('state','=','done')])
        
        
        move_line_pool = self.pool.get('account.move.line')		
        move_pool = self.pool.get('account.move')
        rule_pool = self.pool.get('hr.salary.rule')
        default_aa_id = self._get_analytic_account_id(cr, uid)
        
        journal_id = payslip_obj.browse(cr, uid, payslip_ids)[0].journal_id.id
        period_id = payslip_obj.browse(cr, uid, payslip_ids)[0].period_id.id
        if not period_id:
            date_from = payslip_obj.browse(cr, uid, payslip_ids)[0].date_from
            date_to = payslip_obj.browse(cr, uid, payslip_ids)[0].date_to
            period_id = self._get_period_id(cr, uid, date_from, date_to)
        
        now = datetime.datetime.now()
        date = str(now.year) + "-" + str(now.month) + "-" + str(now.day)
        
        payslip = payslip_obj.browse(cr, uid, payslip_ids)[0]
        move = move_pool.account_move_prepare(cr, uid, journal_id, date, ref=payslip.payslip_run_id.name or payslip.name or 'Payslip', context=context)	
        move.update({
            'period_id' : period_id,
        })
        move_id = move_pool.create(cr, uid, move, context=context)
        
        for payslip in payslip_obj.browse(cr, uid, payslip_ids):
            obj_payslip_line_ids = obj_payslip_line.search(cr, uid, [('slip_id','=', payslip.id)])
            for line in obj_payslip_line.browse(cr, uid, obj_payslip_line_ids):
                name = payslip.name + ' (' +line.salary_rule_id.name+ ')'
            
                #
                # If I have Advances on Payroll I have to manage separatly
                #
                if line.code == 'ADV':
                    adv_o = self.pool.get('isf.hr.payroll.advances')
                    account_o = self.pool.get('account.account')
                    adv_ids = adv_o.search(cr, uid, [('date','>=',date_from),('date','<=',date_to),('employee_id','=',payslip.employee_id.id)])
                    if len(adv_ids) > 0:
                        adv = adv_o.browse(cr, uid, adv_ids)[0]
                        expense_account_id = self.get_default_advances_account_id(cr, uid)
                        account_ids = account_o.search(cr, uid, [('code','=',adv.cash_account)])
                        account = account_o.browse(cr, uid, account_ids)[0]
                        move_line = {
                            'analytic_account_id': adv.analytic_account_id.id, 
                            'tax_code_id': False, 
                            'tax_amount': 0,
                            'ref' : 'Payslip',
                            'name': name or '/',
                            'currency_id': False,
                            'credit': 0.0,
                            'debit': adv.company_amount,
                            'date_maturity' : False,
                            'amount_currency': False,
                            'partner_id': False,
                            'move_id': move_id,
                            'account_id': account.id,
                            'state' : 'valid',
                            'partner_id' : adv.employee_id.address_home_id.id,
                        }

                        result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
                            
                        move_line = {
                            'analytic_account_id': False, 
                            'tax_code_id': False, 
                            'tax_amount': 0,
                            'ref' : 'Payslip',
                            'name': name or '/',
                            'currency_id': False,
                            'credit': adv.company_amount,
                            'debit': 0.0,
                            'date_maturity' : False,
                            'amount_currency': False,
                            'partner_id': False,
                            'move_id': move_id,
                            'account_id': expense_account_id,
                            'state' : 'valid',
                            'partner_id' : adv.employee_id.address_home_id.id,
                        }

                        result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
                else:
                    if line.amount > 0:
                        if line.salary_rule_id.account_credit.id and line.amount > 0.0:
                            move_line = {
                                'analytic_account_id': False, 
                                'tax_code_id': False, 
                                'tax_amount': 0,
                                'ref' : 'Payslip',
                                'name': name or '/',
                                'currency_id': False,
                                'credit': line.amount,
                                'debit': 0.0,
                                'date_maturity' : False,
                                'amount_currency': False,
                                'partner_id': False,
                                'move_id': move_id,
                                'account_id': line.salary_rule_id.account_credit.id,
                                'state' : 'valid'
                            }
		
                            result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
                
                        if line.salary_rule_id.account_debit.id and line.amount > 0.0:
                            aa_id = False
                            if payslip.contract_id.employee_id.department_id:
                                if payslip.contract_id.employee_id.department_id.analytic_account_id:
                                    aa_id = payslip.contract_id.employee_id.department_id.analytic_account_id.id
                            else:
                                if payslip.contract_id.analytic_account_id:
                                    aa_id = payslip.contract_id.analytic_account_id.id
                        
                            # Setting the default Analytic if not defined
                            if aa_id == False:
                                aa_id = default_aa_id
                            
                            move_line = {
                                'analytic_account_id': aa_id, 
                                'tax_code_id': False, 
                                'tax_amount': 0,
                                'ref' : 'Payslip',
                                'name': name or '/',
                                'currency_id': False,
                                'credit': 0.0,
                                'debit': line.amount,
                                'date_maturity' : False,
                                'amount_currency': False,
                                'partner_id': False,
                                'move_id': move_id,
                                'account_id': line.salary_rule_id.account_debit.id,
                                'state' : 'valid'
                            }
		
                            result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
                
            payslip_obj.write(cr, uid, payslip_ids, {'state' : 'done', 'paid' : True,'move_id' : move_id}, context=context)
         
        #if len(payslip_done_ids) > 0:
        #   return self.pool.get('isf.lib.warning').warning(cr, uid, title='Payslip warning', message='Some payslip was unable to write on journal entry')
        
        return True
        
    def cancel(self, cr, uid, ids, context=None):
        return {'type': 'ir.actions.act_window_close'}
    
    def _create_default_journal(self, cr, uid, ids=None, context=None):
        if context is None:
            context = {}
            
        journal_pool = self.pool.get('account.journal')
        journal_ids = journal_pool.search(cr, uid, [('code','=','APRJ')])
        journal_obj = journal_pool.browse(cr, uid, journal_ids, context=context)
        
        Found = False
        for journal in journal_obj:
            if _debug:
                _logger.debug('Found : %d,%s',journal.id, journal.code)
            Found = True
        
        if Found:  
            _logger.debug('Default journal found') 
        else:
            _logger.debug('Default journal not found : create')
            
            seq_pool = self.pool.get('ir.sequence')
            seq_ids = seq_pool.search(cr, uid, [('name','=','Advances Payment Refund Sequence')])
            seq_obj = seq_pool.browse(cr, uid, seq_ids)
            sequence_id = False
            for seq in seq_obj:
                if _debug:
                    _logger.debug('Sequence : %d,%s',seq.id, seq.name)
                sequence_id = seq.id
                break
                
            if not sequence_id:
                seq_vals = {
                    'name' : 'Advances Payment Refund Sequence',
                    'prefix' : 'APR/%(year)s/',
                    'padding' : 4,
                    'implementation' : 'no_gap',
                }
                
                sequence_id = seq_pool.create(cr,uid, seq_vals, context=context) 
            
            journal = {
                'name' : 'Advances Payment Refund Journal',
                'code' : 'APRJ',
                'type' : 'general',
                'sequence_id' : sequence_id,
                'update_posted' : True,
                'analytic_journal_id' : self._get_analytic_journal_by_name(cr, uid, 'General')
            }
            
            journal_pool.create(cr, uid, journal, context=context)
            
        journal_ids = journal_pool.search(cr, uid, [('code','=','PRCJ')])
        journal_obj = journal_pool.browse(cr, uid, journal_ids, context=context)
        
        Found = False
        for journal in journal_obj:
            if _debug:
                _logger.debug('Found : %d,%s',journal.id, journal.code)
            Found = True
        
        if Found:  
            _logger.debug('Default journal found') 
        else:
            _logger.debug('Default journal not found : create')
            
            seq_pool = self.pool.get('ir.sequence')
            seq_ids = seq_pool.search(cr, uid, [('name','=','Payroll Computing Sequence')])
            seq_obj = seq_pool.browse(cr, uid, seq_ids)
            sequence_id = False
            for seq in seq_obj:
                if _debug:
                    _logger.debug('Sequence : %d,%s',seq.id, seq.name)
                sequence_id = seq.id
                break
                
            if not sequence_id:
                seq_vals = {
                    'name' : 'Payroll Computing Sequence',
                    'prefix' : 'PRC/%(year)s/',
                    'padding' : 4,
                    'implementation' : 'no_gap',
                }
                
                sequence_id = seq_pool.create(cr,uid, seq_vals, context=context) 
            
            journal = {
                'name' : 'Payroll Computing Journal',
                'code' : 'PRCJ',
                'type' : 'general',
                'sequence_id' : sequence_id,
                'update_posted' : True,
                'analytic_journal_id' : self._get_analytic_journal_by_name(cr, uid, 'General'),
            }
            
            journal_pool.create(cr, uid, journal, context=context)
            
        journal_ids = journal_pool.search(cr, uid, [('code','=','PRPJ')])
        journal_obj = journal_pool.browse(cr, uid, journal_ids, context=context)
        
        Found = False
        for journal in journal_obj:
            if _debug:
                _logger.debug('Found : %d,%s',journal.id, journal.code)
            Found = True
        
        if Found:  
            _logger.debug('Default journal found') 
        else:
            _logger.debug('Default journal not found : create')
            
            seq_pool = self.pool.get('ir.sequence')
            seq_ids = seq_pool.search(cr, uid, [('name','=','Payroll Payment Sequence')])
            seq_obj = seq_pool.browse(cr, uid, seq_ids)
            sequence_id = False
            for seq in seq_obj:
                if _debug:
                    _logger.debug('Sequence : %d,%s',seq.id, seq.name)
                sequence_id = seq.id
                break
                
            if not sequence_id:
                seq_vals = {
                    'name' : 'Payroll Payment Sequence',
                    'prefix' : 'PRP/%(year)s/',
                    'padding' : 4,
                    'implementation' : 'no_gap',
                }
                
                sequence_id = seq_pool.create(cr,uid, seq_vals, context=context) 
            
            journal = {
                'name' : 'Payroll Payment Journal',
                'code' : 'PRPJ',
                'type' : 'general',
                'sequence_id' : sequence_id,
                'update_posted' : True,
                'analytic_journal_id' : self._get_analytic_journal_by_name(cr, uid, 'General')
            }
            
            journal_pool.create(cr, uid, journal, context=context)
            
        return True
    
    def _get_analytic_journal_by_name(self, cr, uid, name):
        analytic_journal_pool = self.pool.get('account.analytic.journal')
        analytic_journal_ids = analytic_journal_pool.search(cr, uid, [('name','=',name)])
        return analytic_journal_ids[0] if len(analytic_journal_ids) else None
    
isf_hr_payroll()

class hr_payslip_run(osv.osv):
    _inherit = 'hr.payslip.run'
    _description = 'ISF Payroll Sheet'
    
    _columns = {
        'period_id' : fields.many2one('account.period','Period',required=True),
    }
    
    def _get_journal_id(self, cr, uid,context=None):
        journal = self.pool.get('ir.values').get_default(cr, uid, 'isf.hr.payroll','journal_id')
        return journal
    
    _defaults = {
        'journal_id' : _get_journal_id,
    }
    
    def onchange_period(self, cr, uid, ids,period_id):
        period_obj = self.pool.get('account.period')
        period_ids = period_obj.search(cr, uid, [('id','=',period_id)])
        period = period_obj.browse(cr, uid, period_ids)[0]
        
        result = {'value':{}}
        result['value'].update({
            'date_start' : period.date_start,
            'date_end' : period.date_stop,
        })
        
        return result
    
hr_payslip_run()

class hr_payslip(osv.osv):
    _inherit = 'hr.payslip'
    
    _columns = {
        'grade_id' : fields.related('contract_id','type_id',relation='hr.contract.type',type="many2one",string="Grade",store=True),
    }
    
    def _get_company_currency_id(self, cr, uid, context=None):
        users = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        company_id = users.company_id.id
        currency_id = users.company_id.currency_id
		
        return currency_id.id
    
    def compute_sheet(self, cr, uid, ids, context=None):
        #return super(hr_payslip,self).compute_sheet(cr, uid, ids, context=context)
        slip_line_pool = self.pool.get('hr.payslip.line')
        sequence_obj = self.pool.get('ir.sequence')
        total_compute_amount = 0.0
        for payslip in self.browse(cr, uid, ids, context=context):
            number = payslip.number or sequence_obj.get(cr, uid, 'salary.slip')
            #delete old payslip lines
            old_slipline_ids = slip_line_pool.search(cr, uid, [('slip_id', '=', payslip.id)], context=context)
#            old_slipline_ids
            if old_slipline_ids:
                slip_line_pool.unlink(cr, uid, old_slipline_ids, context=context)
            if payslip.contract_id:
                #set the list of contract for which the rules have to be applied
                contract_ids = [payslip.contract_id.id]
            else:
                #if we don't give the contract, then the rules to apply should be for all current contracts of the employee
                contract_ids = self.get_contract(cr, uid, payslip.employee_id, payslip.date_from, payslip.date_to, context=context)
            lines = [(0,0,line) for line in self.pool.get('hr.payslip').get_payslip_lines(cr, uid, contract_ids, payslip.id, context=context)]
            
            #
            # Fixed Incentives and Advances
            #
            period_obj = self.pool.get('account.period')
            inc_obj = self.pool.get('isf.hr.incentive')
            currency_pool = self.pool.get('res.currency')
            rule_o = self.pool.get('hr.salary.rule')
            period_id = period_obj.find(cr, uid, payslip.date_from)
            period = period_obj.browse(cr, uid, period_id)[0]
            adv_o = self.pool.get('isf.hr.payroll.advances')
            adv_ids = adv_o.search(cr, uid, [('period_id','=',period.id),('employee_id','=',payslip.employee_id.id),('state','=','done')])
            
            if _debug:
                _logger.debug('PERIOD : %s', period)
            
            for line in lines:
                rule_id = line[2]['salary_rule_id'];
                rule = rule_o.browse(cr, uid, [rule_id])[0]
                # Incentives
                inc_ids = inc_obj.search(cr, uid, [('employee_id','=',payslip.employee_id.id),('start_period_id.date_start','<=',period.date_start),('end_period_id.date_stop','>=',period.date_stop),('state','=','confirm')])
                for inc in inc_obj.browse(cr, uid, inc_ids):
                    if inc.salary_rule_id.code == rule.code:
                        amount = inc.amount
                        company_currency_id = self._get_company_currency_id(cr, uid)
                        if company_currency_id != inc.currency_id.id:
                            amount = currency_pool.compute(cr, uid, inc.currency_id.id, company_curr_id, inc.amount, context=context)
                        
                        line[2].update({
                            'amount' : amount,
                        })  
                # Advances
                if rule.rule_type == 'adv':
                    adv_amount_total = 0.0
                    for adv in adv_o.browse(cr, uid, adv_ids):
                        adv_amount_total += adv.company_amount
                        
                    line[2].update({
                            'amount' : adv_amount_total,
                    })  
                
                if rule.rule_type == 'net':
                    adv_amount_total = 0.0
                    for adv in adv_o.browse(cr, uid, adv_ids):
                        adv_amount_total += adv.company_amount
                        
                    net_amount = line[2]['amount']
                    net_amount -= adv_amount_total
                    line[2].update({
                            'amount' : net_amount,
                    })  
            
            self.write(cr, uid, [payslip.id], {'line_ids': lines, 'number': number,}, context=context)
       
        self._compute_accounting(cr, uid, ids, period, context=context) 
        
        return True
        
    def get_default_account_moh_income_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.hr.payroll','account_moh_income_id')
        
    def get_default_account_moh_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.hr.payroll','account_moh_id')
        
    def get_default_account_gross_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.hr.payroll','account_gross_id')
    
    def get_default_account_moh_founds_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.hr.payroll','account_moh_founds_id')
    
    def get_default_moh_journal_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.hr.payroll','moh_journal_id')
    
    def _compute_accounting(self, cr, uid, ids, period, context=None):
        if context is None:
            context = {}
        total_amount = 0.0
        ref = 'NA'
        dict_total = {}
        for payslip in self.browse(cr, uid, ids, context=context):
            if payslip.payslip_run_id:
                ref = payslip.payslip_run_id.name
            else:
                ref = payslip.number
                
            total_amount = 0.0
            for line in payslip.line_ids:
                if line.salary_rule_id.rule_type == 'compute':
                    total_amount += line.amount
            
            rec_total = dict_total.get(ref)
            if rec_total is None:
                rec_total = {'amount' : 0.0}
                dict_total.update({ref:rec_total})
                
            rec_total['amount'] += total_amount
            
        if _debug:
            _logger.debug('DICT TOTAL : %s',dict_total)
            
                    
        journal_id = self.get_default_moh_journal_id(cr, uid)
        income_moh_account_id = self.get_default_account_moh_income_id(cr, uid)
        moh_account_id = self.get_default_account_moh_id(cr, uid)
        moh_founds_account_id = self.get_default_account_moh_founds_id(cr, uid)
        gross_account_id = self.get_default_account_gross_id(cr, uid)
        
        move_line_pool = self.pool.get('account.move.line')		
        move_pool = self.pool.get('account.move')
        date = time.strftime("%Y-%m-%d")
        
        period_pool = self.pool.get('account.period')
        period_id = period_pool.find(cr, uid, date, context=context)
        
        ctx = context.copy()
        ctx.update({
            'period_id' : period.id,
        })
       
        for key in dict_total:
            rec = dict_total.get(key)
            total_amount = rec['amount']
            
            move = move_pool.account_move_prepare(cr, uid, journal_id, period.date_stop, ref=key, context=ctx)	
            move_id = move_pool.create(cr, uid, move, context=ctx)
        
            move_line = {
                'analytic_account_id': False, 
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : key,
                'name': 'MoH Founds' or '/',
                'currency_id': False,
                'credit': total_amount,
                'debit': 0.0,
                'date_maturity' : False,
                'amount_currency': False,
                'partner_id': False,
                'move_id': move_id,
                'account_id': moh_founds_account_id,
                'state' : 'valid'
            }
		
            result = move_line_pool.create(cr, uid, move_line,context=ctx,check=False)
        
            move_line = {
                'analytic_account_id': False, 
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : key,
                'name': 'MoH Founds' or '/',
                'currency_id': False,
                'credit': 0.0,
                'debit': total_amount,
                'date_maturity' : False,
                'amount_currency': False,
                'partner_id': False,
                'move_id': move_id,
                'account_id': income_moh_account_id,
                'state' : 'valid'
            }
		
            result = move_line_pool.create(cr, uid, move_line,context=ctx,check=False)
        
            move = move_pool.account_move_prepare(cr, uid, journal_id, period.date_stop, ref=key, context=ctx)	
            move_id = move_pool.create(cr, uid, move, context=ctx)
		
            move_line = {
                'analytic_account_id': False, 
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : key,
                'name': 'Gross Salaries' or '/',
                'currency_id': False,
                'credit': total_amount,
                'debit': 0.0,
                'date_maturity' : False,
                'amount_currency': False,
                'partner_id': False,
                'move_id': move_id,
                'account_id': moh_account_id,
                'state' : 'valid'
            }
		
            result = move_line_pool.create(cr, uid, move_line,context=ctx,check=False)
		
            move_line = {
                'analytic_account_id': False, 
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : key,
                'name': 'Gross Salaries' or '/',
                'currency_id': False,
                'credit': 0.0,
                'debit': total_amount,
                'date_maturity' : False,
                'amount_currency': False,
                'partner_id': False,
                'move_id': move_id,
                'account_id':gross_account_id,
                'state' : 'valid'
            }
		
            result = move_line_pool.create(cr, uid, move_line,context=ctx,check=False)
        
        return True

  
class hr_salary_rule(osv.Model):
    _inherit = 'hr.salary.rule'
    
    _columns = {
        'rule_type' : fields.selection([('normal','Normal'),('compute','Compute'),('net','Net'),('adv','Advances')],string="Rule Type",required=True),
    }
    
    _defaults = {
        'rule_type' : 'normal',
    }

hr_salary_rule()
    