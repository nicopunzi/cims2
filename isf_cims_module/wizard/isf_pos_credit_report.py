from openerp.osv import orm, fields, osv
from openerp import netsvc

import datetime

class isf_pos_credit_wizard(osv.osv_memory):
    _name = "isf.pos.credit.wizard"
    _description = "Credit Report"
    _columns = {
        'date_start' : fields.date('Date Start', required=True),
        'date_stop' : fields.date('Date End', required=True),
        'customer_id' : fields.many2one('res.partner','Customer',required=True)
    }
    
    _defaults = {
        'date_start' : fields.date.context_today,
        'date_stop' : fields.date.context_today,
    }
    
    
isf_pos_credit_wizard()
