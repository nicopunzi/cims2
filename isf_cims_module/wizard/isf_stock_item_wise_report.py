from openerp.osv import orm, fields, osv
from openerp import netsvc

import datetime

class isf_stock_item_wise_report(osv.TransientModel):
    _name = "isf.stock.item.wise.report"
    _description = "Stock Sale Summary Report"
    _columns = {
        'date_start' : fields.date('Date Start',required=True),
        'date_end' : fields.date('Date End',required=True),
    }
    
    _defaults = {
        'date_start' : fields.date.context_today,
        'date_end' : fields.date.context_today,
    }
