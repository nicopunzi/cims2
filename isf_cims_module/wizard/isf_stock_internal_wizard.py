from openerp.osv import orm, fields, osv
from openerp import netsvc

import datetime

class isf_stock_internal_wizard(osv.TransientModel):
    _name = "isf.stock.internal.wizard"
    _description = "Stock Internal"
    _columns = {
        'date_start' : fields.date('Date Start',required=True),
        'date_stop' : fields.date('Date End',required=True),
        'location_id' : fields.many2one('stock.location',string="Stock Location", required=True),
    }
    
    _defaults = {
        'date_start' : fields.date.context_today,
        'date_stop' : fields.date.context_today,
    }
    
isf_stock_internal_wizard()
