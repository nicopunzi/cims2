from openerp.osv import fields, osv
from openerp.tools.translate import _
import logging

_logger = logging.getLogger(__name__)
_debug=False

class stock_picking(osv.osv):
    _inherit = 'stock.picking'
    _name = 'stock.picking'
    
    def action_invoice_create_ex(self, cr, uid, ids, journal_id=False,
            group=False, document_number=False, type='out_invoice', context=None):
        """ Creates invoice based on the invoice state selected for picking.
        @param journal_id: Id of journal
        @param group: Whether to create a group invoice or not
        @param type: Type invoice to be created
        @return: Ids of created invoices for the pickings
        """
        
        _logger.info('==> Action invoice create')
        
        if context is None:
            context = {}
            
        if _debug:
            _logger.debug('Context : %s', context)

        order_obj = self.pool.get('purchase.order')
        invoice_obj = self.pool.get('account.invoice')
        invoice_line_obj = self.pool.get('account.invoice.line')
        partner_obj = self.pool.get('res.partner')
        invoices_group = {}
        res = {}
        inv_type = type
        
        currency_id = False
        order_id = context.get('order_id')

        if order_id:
            order_ids = order_obj.search(cr, uid, [('id','=',order_id)])
            order = order_obj.browse(cr, uid, order_ids)[0]
            currency_id = order.order_currency_id.id
        
        for picking in self.browse(cr, uid, ids, context=context):
            if picking.invoice_state != '2binvoiced':
                continue
            partner = self._get_partner_to_invoice(cr, uid, picking, context=context)
            if isinstance(partner, int):
                partner = partner_obj.browse(cr, uid, [partner], context=context)[0]
            if not partner:
                raise osv.except_osv(_('Error, no partner!'),
                    _('Please put a partner on the picking list if you want to generate invoice.'))

            if not inv_type:
                inv_type = self._get_invoice_type(picking)

            if group and partner.id in invoices_group:
                invoice_id = invoices_group[partner.id]
                invoice = invoice_obj.browse(cr, uid, invoice_id)
                invoice_vals_group = self._prepare_invoice_group(cr, uid, picking, partner, invoice, context=context)
                invoice_vals_group['document_number'] = document_number
                invoice_vals_group['currency_id'] = currency_id
                invoice_obj.write(cr, uid, [invoice_id], invoice_vals_group, context=context)
            else:
                invoice_vals = self._prepare_invoice(cr, uid, picking, partner, inv_type, journal_id, context=context)
                invoice_vals['document_number'] = document_number
                invoice_vals['currency_id'] = currency_id
                invoice_id = invoice_obj.create(cr, uid, invoice_vals, context=context)
                invoices_group[partner.id] = invoice_id
            res[picking.id] = invoice_id
            for move_line in picking.move_lines:
                if move_line.state == 'cancel':
                    continue
                if move_line.scrapped:
                    # do no invoice scrapped products
                    continue
                vals = self._prepare_invoice_line(cr, uid, group, picking, move_line,
                                invoice_id, invoice_vals, context=context)
                if vals:
                    invoice_line_id = invoice_line_obj.create(cr, uid, vals, context=context)
                    self._invoice_line_hook(cr, uid, move_line, invoice_line_id)

            invoice_obj.button_compute(cr, uid, [invoice_id], context=context,
                    set_total=(inv_type in ('in_invoice', 'in_refund')))
            self.write(cr, uid, [picking.id], {
                'invoice_state': 'invoiced',
                }, context=context)
            self._invoice_hook(cr, uid, picking, invoice_id)
        self.write(cr, uid, res.keys(), {
            'invoice_state': 'invoiced',
            }, context=context)
        return res

stock_picking()

class isf_stock_invoice_onshipping(osv.osv_memory):
    _inherit = 'stock.invoice.onshipping'
    
    _columns = {
        'document_number' : fields.char('Document Number', required=True),
        'analytic_account' : fields.many2one('account.analytic.account', 'Analytic Account',required=False),
    }
    
    
    def create_invoice(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        picking_pool = self.pool.get('stock.picking')
        onshipdata_obj = self.read(cr, uid, ids, ['journal_id', 'group', 'invoice_date','document_number','analytic_account'])
        if context.get('new_picking', False):
            onshipdata_obj['id'] = onshipdata_obj.new_picking
            onshipdata_obj[ids] = onshipdata_obj.new_picking
        context['date_inv'] = onshipdata_obj[0]['invoice_date']
        active_ids = context.get('active_ids', [])
        active_picking = picking_pool.browse(cr, uid, context.get('active_id',False), context=context)
        inv_type = picking_pool._get_invoice_type(active_picking)
        context['inv_type'] = inv_type
        if isinstance(onshipdata_obj[0]['journal_id'], tuple):
            onshipdata_obj[0]['journal_id'] = onshipdata_obj[0]['journal_id'][0]
        ctx = context.copy()
        ctx.update({
            'order_id' : active_picking.purchase_id.id,
            })
        res = picking_pool.action_invoice_create_ex(cr, uid, active_ids,
              journal_id = onshipdata_obj[0]['journal_id'],
              group = onshipdata_obj[0]['group'],
              document_number = onshipdata_obj[0]['document_number'],
              type = inv_type,
              context=ctx)
        
        if res:      
            # Add document number and Analytic Account to invoice
            analytic_account = onshipdata_obj[0]['analytic_account']
            
            if _debug:
                _logger.debug('Res : %s', res)
                _logger.debug('Analytic : %s', analytic_account)
        
            for invoice_id in res.values():
                invoice_obj = self.pool.get('account.invoice')
            
                invoice_line_obj = self.pool.get('account.invoice.line')
                invoice_line_ids = invoice_line_obj.search(cr, uid, [('invoice_id','=',invoice_id)])
                
                if analytic_account:
                    for line in invoice_obj.browse(cr, uid, invoice_line_ids, context=context):
                        invoice_line_obj.write(cr, uid, line.id, {'account_analytic_id' : analytic_account[0] }, context=context)
        return res

    
isf_stock_invoice_onshipping()
