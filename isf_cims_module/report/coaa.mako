<html>
<head>
    <style type="text/css">
        ${css}
    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <h1>Chart of Analytic Accounts</h1>
    <div class="no_bloc"></div>
    <%
    level = -1
    %>
    <table style="width: 100%">
        <tr class="table_header">
            <td class="label">Analytic Account</td>
            <td class="currency">Currency</td>
        </tr>
       <tr class="row">
            <td colspan="2">
                <table class="tr_bottom_line"></table>
            </td>
        </tr>
        %for account, alevel in lines():
            <tr class="row" style="border-top: 0.5px solid blue;">
                <td class="label">
	                %for l in range(alevel):
	                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                %endfor
	                <% level = alevel %>
	                %if account['code'] == '0':
	                    <b>${account['name']}</b>
	                %else:
	                	%if 'view' in account['type']: 
	                    	<b>${account['code']} - ${account['name']}</b>
	                   	%else:
	                   		${account['code']} - ${account['name']}
	                   	%endif
	                %endif
               </td>
                <td class="currency">
                    ${account['currency_id']['name']}
                </td>
            </tr>
        %endfor
    </table>
</body>
</html>
