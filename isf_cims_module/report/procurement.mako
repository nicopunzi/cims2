<html>
<head>
    <style type="text/css">
        ${css}
        
.ProductTable {
	margin:0px;padding:0px;
	width:100%;
	border:1px solid #000000;
	
	-moz-border-radius-bottomleft:0px;
	-webkit-border-bottom-left-radius:0px;
	border-bottom-left-radius:0px;
	
	-moz-border-radius-bottomright:0px;
	-webkit-border-bottom-right-radius:0px;
	border-bottom-right-radius:0px;
	
	-moz-border-radius-topright:0px;
	-webkit-border-top-right-radius:0px;
	border-top-right-radius:0px;
	
	-moz-border-radius-topleft:0px;
	-webkit-border-top-left-radius:0px;
	border-top-left-radius:0px;
}.ProductTable table{
    border-collapse: collapse;
        border-spacing: 0;
	width:100%;
	height:100%;
	margin:0px;padding:0px;
}.ProductTable tr:last-child td:last-child {
	-moz-border-radius-bottomright:0px;
	-webkit-border-bottom-right-radius:0px;
	border-bottom-right-radius:0px;
}
.ProductTable table tr:first-child td:first-child {
	-moz-border-radius-topleft:0px;
	-webkit-border-top-left-radius:0px;
	border-top-left-radius:0px;
}
.ProductTable table tr:first-child td:last-child {
	-moz-border-radius-topright:0px;
	-webkit-border-top-right-radius:0px;
	border-top-right-radius:0px;
}.ProductTable tr:last-child td:first-child{
	-moz-border-radius-bottomleft:0px;
	-webkit-border-bottom-left-radius:0px;
	border-bottom-left-radius:0px;
}.ProductTable tr:hover td{
	background-color:#ffffff;
		

}
.ProductTable td{
	vertical-align:middle;
	
	background-color:#ffffff;

	border:1px solid #000000;
	border-width:0px 1px 1px 0px;
	text-align:left;
	padding:7px;
	font-size:14px;
	font-family:Arial;
	font-weight:normal;
	color:#000000;
}.ProductTable tr:last-child td{
	border-width:0px 1px 0px 0px;
}.ProductTable tr td:last-child{
	border-width:0px 0px 1px 0px;
}.ProductTable tr:last-child td:last-child{
	border-width:0px 0px 0px 0px;
}
.ProductTable tr:first-child td{
		background:-o-linear-gradient(bottom, #4c4c4c 5%, #4c4c4c 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #4c4c4c), color-stop(1, #4c4c4c) );
	background:-moz-linear-gradient( center top, #4c4c4c 5%, #4c4c4c 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#4c4c4c", endColorstr="#4c4c4c");	background: -o-linear-gradient(top,#4c4c4c,4c4c4c);

	background-color:#4c4c4c;
	border:0px solid #000000;
	text-align:center;
	border-width:0px 0px 1px 1px;
	font-size:14px;
	font-family:Arial;
	font-weight:bold;
	color:#ffffff;
}
.ProductTable tr:first-child:hover td{
	background:-o-linear-gradient(bottom, #4c4c4c 5%, #4c4c4c 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #4c4c4c), color-stop(1, #4c4c4c) );
	background:-moz-linear-gradient( center top, #4c4c4c 5%, #4c4c4c 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#4c4c4c", endColorstr="#4c4c4c");	background: -o-linear-gradient(top,#4c4c4c,4c4c4c);

	background-color:#4c4c4c;
}
.ProductTable tr:first-child td:first-child{
	border-width:0px 0px 1px 0px;
}
.ProductTable tr:first-child td:last-child{
	border-width:0px 0px 1px 1px;
}

    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print lines()
    """
    %>
    
    <div>
    <center>
    <h2><b>
    %for header in headers():
        Preparement Paper for ${header['date']}
    %endfor
    </b></h2>
    </center>
    </div>
    
    <br/>
    
    <div class="ProductTable">
        <table style="width: 100%" border="0">
            <tr>
                <td>Product</td>
                <td>Sold</td>
                <td>FoC</td>
                <td>Credit</td>
                <td>Total</td>
            </tr>
            %for data in lines():
                <tr>
                    <td>${data['name']}</td>
                    <td style="text-align: right;">${data['sold']}</td>
                    <td style="text-align: right;">${data['foc']}</td>
                    <td style="text-align: right;">${data['credit']}</td>
                    <td style="text-align: right;"><b>${data['total']}</b></td>
                </tr>
            %endfor
        </table>
    </div>
</body>
</html>
