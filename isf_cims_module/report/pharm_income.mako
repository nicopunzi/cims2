<html>
<head>
    <style type="text/css">
        ${css}
        
@media print {
   thead {display: table-header-group;}
}
        
.break{
    display: block;
    clear: both;
    page-break-after: always;
}

.table-data {
    border: 1px solid black;
    border-collapse: collapse;
    width:100%;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.table-data-nb {
    border: 0px;
    border-collapse: collapse;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.td-data {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.td-data-nb {
    border: 0px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.th-data {
    font-family:verdana;
    font-size:10px;
}


    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print headers()
    print lines()
    """
    %>
    <center>
        <h3>Daily Income</h3>
    </center>
    %for data in headers():
    <center>
        <table class="table-data" width="70%">
            <tr>
                <th class="td-data" style="width: 40%;">Department</th>
                <th class="td-data" style="width: 40%;">Date</th>
            </tr>
            <tr>
                <td class="td-data" style="width: 30%;text-align:left;"><b>Pharmacy</b></td>
                <td class="td-data" style="width: 30%;text-align:right;">${data['date']}</td>
            </tr>
        </table>

        <br/>
        <table class="table-data" width="70%">
            <tr>
                <th class="td-data" style="width: 20%;">Type</th>
                <th class="td-data" style="width: 40%;">Amount</th>
                <th class="td-data" style="width: 40%;">Amount Free</th>
            </tr>
            %for data in lines():
                %if data['is_total'] == '0':
                <tr>
                    <td class="td-data" style="width: 20%;text-align:left;"><b>${data['type']}</b></td>
                    <td class="td-data" style="width: 40%;text-align:right;">${data['amount']}</td>
                    <td class="td-data" style="width: 40%;text-align:right;">${data['amount_free']}</td>
                </tr>
                %endif
                %if data['is_total'] == '1':
                <tr>
                    <td class="td-data" style="width: 20%;text-align:left;"><b>Totals : </b></td>
                    <td class="td-data" style="width: 40%;text-align:right;"><b>${data['amount']}</b></td>
                    <td class="td-data" style="width: 40%;text-align:right;"><b>${data['amount_free']}</b></td>
                </tr>
                %endif
            %endfor
        </table>
    </center>
    %endfor
</body>
</html>
