<html>
<head>
    <style type="text/css">
        ${css}
        
@media print {
   thead {display: table-header-group;}
}
        
td {
    font-size: 14px,
}

table { 
    border-collapse: collapse; 
}


    </style>
</head>
<body>
    <%page expression_filter="entity"/>
     <h1><b><i>Stock Disposal</i></b></h1>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print headers()
    print lines()
    """
    %>
    
    
   
        
        <table style="width: 100%" border="0">
            %for head in headers():
                <tr>
                    <td><b>ID : </b></td><td>${head['sequence_id']}</td>
                </tr>
                <tr>
                    <td><b>Description : </b></td><td>${head['desc']}</td>
                </tr>
                <tr>
                    <td><b>Location :</b></td><td>${head['location']}</td>
                </tr>
                <tr>
                    <td><b>Type : </b></td><td>${head['type']}</td>
                </tr>
                <tr>
                    <td><b>Date :</b></td><td>${head['date']}</td>
                </tr>
            %endfor
        </table>
        <br/>
        <table style="width: 100%" border="0">
            <tr class="table_header" style="border-bottom: 0.5px solid black">
                <td>Lot</td>
                <td>Expiry Date</td>
                <td>Product</td>
                <td>Qty</td>
            </tr>
            %for line in lines():
                %if line['lot_id'] != 'total':
                    <tr class="row" style="background-color:#F2F2F2">
                        <td>${line['lot_id']}</td>
                        <td>${line['expiry_date']}</td>
                        <td>${line['product']}</td>
                        <td>${line['qty']}</td>
                    </tr>
                %endif
            %endfor
        </table>
        

</body>
</html>
