import time
from openerp.report import report_sxw
from openerp.osv import fields, osv
import logging
import locale

_logger = logging.getLogger(__name__)
_debug=False

class isf_stock_inventory_move_view(osv.osv_memory):
    _name = "isf.stock.inventory.move.view"
    _description = "Stock Inventory Move"
    
    def _get_server_locale(self, cr, uid, context=None):
        server_locale = locale.getdefaultlocale()[0]
        if _debug:
            _logger.debug('==> in _get_server_locale : locale.getlocale = %s', server_locale)
        
        server_lang = self.pool.get('res.lang').search(cr, uid, [('code','=',server_locale)])
        
        if not server_lang:
            if _debug:
                _logger.debug('==> in _get_server_locale : server_lang %s not intalled, fallback on user locale', server_locale)
            lang_id = self._get_user_locale(cr, uid, context)
        else:
            lang_id = server_lang[0]
            
        return lang_id
    
    def _get_user_locale(self, cr, uid, context=None):
        current_user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        if _debug:
            _logger.debug('==> in _get_user_locale : current_user = %s', current_user)
        
        lang = current_user.lang
        if _debug:
            _logger.debug('==> in _get_user_locale : current_user.lang = %s', lang)
        
        lang_id = self.pool.get('res.lang').search(cr, uid, [('code','=',lang)])[0]
        return lang_id
     
    _columns = {
        'message': fields.char("Message"),
        'report_id': fields.many2one('ir.actions.report.xml', 'Available Reports',
                                      domain=[('model','=','stock.inventory'),('report_type','=','webkit')],
                                      required=True),
        'report_name': fields.related('report_id','report_name', readonly=True, type='char', 
                                      relation='ir.actions.report.xml', string='Report Name'),
        'locale_id': fields.many2one('res.lang','Available formats', required=True),
        'locale_code': fields.related('locale_id','code', readonly=True, type='char', 
                                      relation='res.lang', string='Locale Code'),
    }
    _defaults = {
        'message': 'Please select a report and change the format according with your computer regional settings',
        'locale_id': _get_server_locale,
    }
    
    def launch_report(self, cr, uid, ids, context=None):
        if _debug:
            _logger.debug('==> in launch_report : context = %s', context)
        context.update({
            'lang': context.get('locale_code'),
            })
        if _debug:
            _logger.debug('==> in launch_report : updated context = %s', context)
        return {
            'type': 'ir.actions.report.xml',
            'report_name': context.get('report_name'),
            'context': context,
        }

class stock_inventory_move(report_sxw.rml_parse):
    _inherit = 'stock.inventory.move'
    
    def __init__(self, cr, uid, name, context):
        if _debug:
            _logger.debug('==> in init : context = %s', context)
        super(stock_inventory_move, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
             'time': time,
             'total_price':self._price_total,
             'qty_total':self._qty_total,
        })

    
    def _price_total(self, objects):
        total = 0.0
        uom = objects[0].product_uom.name
        for obj in objects:
            total += obj.price
        return {'price':total}
        
    def _qty_total(self, objects):
        total = 0.0
        for obj in objects:
            factor = obj.product_uom.factor
            total += obj.product_qty / factor
        return {'quantity':total}


    
report_sxw.report_sxw(
    'report.isf.stock.inventory.move',
    'stock.inventory',
    'addons/isf_cims_module/report/stock_inventory_move.mako',
    parser=stock_inventory_move,
    header='internal'
)
