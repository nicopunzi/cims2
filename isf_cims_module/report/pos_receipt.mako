<html>
<head>
    <style type="text/css">
        ${css}
        
@media print {
   thead {display: table-header-group;}
}
        
.break{
    display: block;
    clear: both;
    page-break-after: always;
}

.table-data {
    border: 1px solid black;
    border-collapse: collapse;
    width:100%;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.table-data-nb {
    border: none;
    font-family:verdana;
    width:100%;
    font-size:8px;
    padding: 5px;
}

.td-data {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:8px;
}

.td-data-nb {
    border: none,
    padding: 5px;
    font-family:verdana;
    font-size:8px;
}

.th-data {
    font-family:verdana;
    font-size:10px;
}

    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print lines()
    """
    %>
        <table class="table-data">
            %for data in headerlines():
                <tr>
                    <th class="td-data" style="width: 30%;">Number</th>
                    <th class="td-data" style="width: 30%;">ID</th>
                    <th class="td-data" style="width: 20%;">PoS</th>
                    <th class="td-data" style="width: 20%;">Type</th>
                </tr>
                <tr>
                    <td class="td-data" style="width: 30%;text-align: left;">${data['document_number']}</td>
                    <td class="td-data" style="width: 30%;text-align: left;">${data['pos_id']}</td>
                    <td class="td-data" style="width: 20%;text-align: right;">${data['pos']}</td>
                    <td class="td-data" style="width: 20%;text-align: right;">${data['type']}</td>
                </tr>
            %endfor
        </table>
        <table class="table-data-nb">
            <tr>
                <td class="td-data-nb" style="width: 100%;text-align: left;">${data['description']}</td>
            </tr>
        </table>

        <br/>
    
        <table class="table-data">
            <tr>
                <th class="td-data" style="width: 40%;">Product</th>
                <th class="td-data" style="width: 10%;">Quantity</th>
                <th class="td-data" style="width: 50%;">Unit Price</th>
            </tr>
            %for data in lines():
                %if data['is_total'] == '0':
                <tr>
                    <td class="td-data" style="width: 40%;text-align: left;">${data['product']}</td>
                    <td class="td-data" style="width: 10%;text-align: right;">${data['qty']}</td>
                    <td class="td-data" style="width: 50%;text-align: right;">${data['price']} ${data['currency']}</td>
                </tr>
                %endif
                %if data['is_total'] == '1':
                <tr>
                    <td class="td-data" style="width: 40%;text-align: left;"/>
                    <td class="td-data" style="width: 10%;text-align: right;"><b>${data['product']}</b></td>
                    <td class="td-data" style="width: 50%;text-align: right;"><b>${data['price']} ${data['currency']}</b></td>
                </tr>
                %endif
                %if data['is_total'] == '2':
                <tr>
                    <td class="td-data" style="width: 40%;text-align: left;"/>
                    <td class="td-data" style="width: 20%;text-align: right;">${data['product']}</td>
                    <td class="td-data" style="width: 40%;text-align: right;">${data['price']} ${data['currency']}</td>
                </tr>
                %endif
            %endfor
        </table>
</body>
</html>
