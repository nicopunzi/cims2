<html>
<head>
    <style type="text/css">
        ${css}
        
@media print {
   thead {display: table-header-group;}
}
        
.break{
    display: block;
    clear: both;
    page-break-after: always;
}

.table-data {
    border: 1px solid black;
    border-collapse: collapse;
    width:100%;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.table-data-nb {
    border: 0px;
    border-collapse: collapse;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.td-data {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.td-data-nb {
    border: 0px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.th-data {
    font-family:verdana;
    font-size:10px;
}


    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print tableheaders()
    print headers()
    print lines()
    """
    %>
    <h3>
        Stock Balance Report
    </h3>
    <center>
    <table class="table-data">
            <tr>
                <th class="td-data" style="width: 50%;">Location</th>
                <th class="td-data" style="width: 50%;">Date</th>
            </tr>
        %for head in headers():
            <tr>
                <td class="td-data" style="width: 50%;">${head['location']}</td>
                <td class="td-data" style="width: 50%;text-align:right">${head['date']}</td>
            </tr>
        %endfor
    </table>
    </center>
            
    <br/>

    <div>
        <table  class="table-data">
                <tr>
                    <th class="td-data" style="width: 10%;">Stock Code</th>
                    <th class="td-data" style="width: 10%;">Code</th>
                    <th class="td-data" style="width: 40%;">Product</th>
                    <th class="td-data" style="width: 20%;">Quantity</th>
                    <th class="td-data" style="width: 20%;">Expiry Date</th>
                </tr>
                %for data in lines():
                    %if data['expired'] == '0':
                        <tr>
                            <td class="td-data" style="width: 10%;">${data['code']}</td>
                            <td class="td-data" style="width: 10%;text-align:right">${data['p_code']}</td>
                            <td class="td-data" style="width: 40%;">${data['name']}</td>
                            <td class="td-data" style="width: 20%;text-align:right">${data['qty']}</td>
                            <td class="td-data" style="width: 20%;text-align:right">${data['expiry_date']}</td>
                        </tr>
                    %endif
                    %if data['expired'] == '1':
                        <tr>
                            <td class="td-data" style="width: 10%;color:red;">${data['code']}</td>
                            <td class="td-data" style="width: 10%;color:red;text-align:right">${data['p_code']}</td>
                            <td class="td-data" style="width: 40%;color:red;">${data['name']}</td>
                            <td class="td-data" style="width: 20%;color:red;text-align:right">${data['qty']}</td>
                            <td class="td-data" style="width: 20%;color:red;text-align:right">${data['expiry_date']}</td>
                        </tr>
                    %endif
                %endfor
        </table>
    </div>
        
</body>
</html>
