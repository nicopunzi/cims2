# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug=False



class isf_stock_item_wise_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.stock.item.wise.webkit'

    def __init__(self, cr, uid, name, context=None):
    
        super(isf_stock_item_wise_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'headers' : self.headers,
        })
        self.context = context
        self.result_acc = []
        self.headers_acc = []

    """"
    def set_context(self, objects, data, ids, report_type=None):
        for obj in objects:
            print obj, obj.id
        new_ids = ids
        return super(isf_bs_report_parser, self).set_context(objects, data, new_ids, report_type=report_type)
    """
    #def _add_header(self, node, header=1):
    #    if header == 0:
    #        self.rml_header = ""
    #    return True
    
    
    def headers(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.item.wise.report')
        date_s_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_start'])
        date_e_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_end'])
        date_start = date_s_obj[0]['date_start']
        date_end = date_e_obj[0]['date_end']
        
        res = { 
            'date_start' : date_start,
            'date_end' : date_end,
        }
        self.headers_acc.append(res)
        
        return self.headers_acc
    
    def _manage_generic_pos(self, cr, uid, shop_id, date_start, date_end):
        result_p = {}
        result_s = {}
        data = []
        pos_o = self.pool.get('isf.pos')
        pos_ids = pos_o.search(cr, uid, [('date','>=',date_start),('date','<=',date_end),('state','=','confirm'),('shop_id','=',shop_id),('product_type_sel','=','product'),('transaction_type_product','in',['sale','credit'])])
        
        total_sales_p = total_sales_qty_p = 0
        for pos in pos_o.browse(cr, uid, pos_ids):
            for line in pos.product_ids:
                rec = result_p.get(line.product_id.name_template)
                if rec is None:
                    rec = {'product': line.product_id.name_template, 'code':line.product_id.default_code, 
                        'sales_qty' : 0, 'foc_qty' : 0, 'internal_qty' : 0, 'total_qty' : 0, 'type' : 'Stock',
                        'sales' : 0, 'foc': 0, 'internal' : 0,'is_total' : False,'location':pos.shop_id.name,'is_header':False}
                    result_p.update({line.product_id.name_template:rec})
                if pos.transaction_type_product in ['sale','credit']:
                    rec['sales'] += (line.quantity * line.unit_price)
                    total_sales_p += (line.quantity * line.unit_price)
                    total_sales_qty_p += line.quantity
                    rec['sales_qty'] += line.quantity
                    rec['total_qty'] = rec['sales_qty']

        total_sales_s = total_sales_qty_s = 0
        pos_ids = pos_o.search(cr, uid, [('date','>=',date_start),('date','<=',date_end),('state','=','confirm'),('product_type_sel','=','service'),('shop_id','=',shop_id),('transaction_type_service','in',['sale','credit'])])
        for pos in pos_o.browse(cr, uid, pos_ids):
            for line in pos.service_ids:
                rec = result_s.get(line.service_id.name_template)
                if rec is None:
                    rec = {'product': line.service_id.name_template, 'code':line.service_id.default_code or '', 
                        'sales_qty' : 0, 'foc_qty' : 0, 'internal_qty' : 0, 'total_qty' : 0, 'type' : 'Service',
                        'sales' : 0, 'foc': 0, 'internal' : 0,'is_total' : False,'is_header':False,'location':pos.shop_id.name}
                    result_s.update({line.service_id.name_template:rec})
                if pos.transaction_type_product in ['sale','credit']:
                    rec['sales'] += (line.quantity * line.unit_price)
                    total_sales_s += (line.quantity * line.unit_price)
                    total_sales_qty_s += line.quantity
                    rec['sales_qty'] += line.quantity
                    rec['total_qty'] = rec['sales_qty']

        
        # Manage product result           
        list_key = result_p.keys()
        list_key.sort()
        for key in list_key:
            rec = result_p.get(key)
            rec.update({
                'sales' : locale.format("%20d",rec['sales'], grouping=True),
                'sales_qty' : rec['sales_qty'],
            })
            data.append(rec)
        if len(data) > 0:
            rec = {
                'product': False,
                'sales' : locale.format("%20d",total_sales_p, grouping=True), 
                'sales_qty' : total_sales_qty_p,
                'is_total' : True,
                'type' : False,
                'location' : False,
                'is_header' : False,
                'code' : False,
                }
            data.append(rec)
            data[0].update({'is_header':True})
        # Manage service result  
        first = True
        list_key = result_s.keys()
        list_key.sort()
        for key in list_key:
            rec = result_s.get(key)
            rec.update({
                'sales' : locale.format("%20d",rec['sales'], grouping=True),
                'sales_qty' : rec['sales_qty'],
            })
            if first:
                rec.update({'is_header':True})
                first = False
            data.append(rec)
        if len(data) > 0:
            rec = {
                'product': False,
                'sales' : locale.format("%20d",total_sales_s, grouping=True), 
                'sales_qty' : total_sales_qty_s,
                'is_total' : True,
                'type' : False,
                'location' : False,
                'is_header' : False,
                'code' : False,
                }
            data.append(rec)
        return data
        
    def _manage_pharmacy_pos(self, cr, uid, date_start, date_end):
        result = {}
        data = []
        pharm_o = self.pool.get('isf.pharmacy.pos')
        pharm_ids = pharm_o.search(cr, uid, [('payment_date','>=',date_start),('payment_date','<=',date_end),('state','=','close'),('type','in',['paid','credit'])])
        
        total_sales = total_foc = total_internal = 0
        total_sales_qty = total_foc_qty = total_internal_qty = 0
        for pharm in pharm_o.browse(cr, uid, pharm_ids):
            for line in pharm.product_line_ids:
                rec = result.get(line.product.name_template)
                if rec is None:
                    rec = {'product': line.product.name_template, 'code':line.product.default_code, 'type' : 'Stock',
                        'sales_qty' : 0, 'foc_qty' : 0, 'internal_qty' : 0, 'total_qty' : 0, 'location' : 'PHARMACY',
                        'sales' : 0, 'foc': 0, 'internal' : 0,'is_total' : False,'is_header' : False}
                    result.update({line.product.name_template:rec})
                if pharm.type in ['paid','credit']:
                    rec['sales'] += (line.amount * line.price)
                    total_sales += (line.amount * line.price)
                    total_sales_qty += line.amount
                    rec['sales_qty'] += line.amount
                    rec['total_qty'] = rec['sales_qty'] + rec['foc_qty'] + rec['internal_qty']
                    
        list_key = result.keys()
        list_key.sort()
        for key in list_key:
            rec = result.get(key)
            rec.update({
                'sales' : locale.format("%20d",rec['sales'], grouping=True),
                'sales_qty' : rec['sales_qty'],
            })
            data.append(rec)
        if len(data) > 0:
            rec = {'product': False,
                'sales' : locale.format("%20d",total_sales, grouping=True), 
                'sales_qty' : total_sales_qty,
                'is_total' : True,
                'type' : False,
                'location' : False,
                'is_header' : False,
                'code' : False,
            }
            data.append(rec)
            data[0].update({'is_header':True})
        return data
    
    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.item.wise.report')
        
        date_s_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_start'])
        date_start = date_s_obj[0]['date_start']
        date_e_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_end'])
        date_end = date_e_obj[0]['date_end']
       

        res = []
        shop_o = self.pool.get('isf.shop')
        shop_ids = shop_o.search(self.cr,self.uid,[])
        for shop in shop_o.browse(self.cr, self.uid, shop_ids):
            if _debug:
                _logger.debug('Manage PoS')
            res = self._manage_generic_pos(self.cr, self.uid, shop.id, date_start, date_end)
            for r in res:
                self.result_acc.append(r)
        if _debug:
            _logger.debug('Manage Pharmacy')
        res = self._manage_pharmacy_pos(self.cr, self.uid, date_start, date_end)
        for r in res:
            self.result_acc.append(r)

        return self.result_acc
        

report_sxw.report_sxw('report.isf.stock.item.wise.webkit', 'isf.stock.item.wise.report', 'addons/isf_cims_module/report/item_wise.mako', parser=isf_stock_item_wise_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
