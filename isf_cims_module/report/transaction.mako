<html>
<head>
    <style type="text/css">
        ${css}
        
@media print {
   thead {display: table-header-group;}
}
        
.break{
    display: block;
    clear: both;
    page-break-after: always;
}

.table-data {
    border: 1px solid black;
    border-collapse: collapse;
    width:100%;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.table-data-nb {
    border: 0px;
    border-collapse: collapse;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.td-data {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.td-data-dashed {
    border: 1px solid black;
    border-collapse: collapse;
    border-style: dashed;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.td-data-nb {
    border: 0px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.th-data {
    font-family:verdana;
    font-size:10px;
}


    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print headers()
    print lines()
    """
    %>
    <h3>Detailed Sales Transaction Report</h3>
    %for data in headers():
    <center>
        <table class="table-data" width="70%">
            <tr>
                <th class="td-data" style="width: 40%;">Location</th>
                <th class="td-data" style="width: 30%;">Date Start</th>
                <th class="td-data" style="width: 30%;">Date Stop</th>
            </tr>
            <tr>
                <td class="td-data" style="width: 40%;"><b>${data['store']}</b></th>
                <td class="td-data" style="width: 30%;text-align:right;">${data['date_start']}</td>
                <td class="td-data" style="width: 30%;text-align:right;">${data['date_end']}</td>
            </tr>
        </table>
    </center>
    %endfor
    <br/>
    <table class="table-data" width="100%">
        <tr>
            <th class="td-data" style="width: 10%;">Type</th>
            <th class="td-data" style="width: 10%;">Curr</th>
            <th class="td-data" style="width: 15%;">Date</th>
            <th class="td-data" style="width: 15%;">Trans#</th>
            <th class="td-data" style="width: 15%;">Reference</th>
            <th class="td-data" style="width: 20%;">Amount</th>
            <th class="td-data" style="width: 15%;">Curr. Amount</th>
        </tr>
        %for data in lines():
            %if data['is_total'] == False:
                %if data['print_header'] == True:
                <tr>
                    <td class="td-data" style="width: 10%;"><b>${data['type']}</b></td>
                    <td class="td-data" style="width: 10%;">${data['currency']}</td>
                    <td class="td-data" style="width: 80%;" colspan="5"><b>${data['store']} - ${data['product_type']}</b></td>
                </tr>
                <tr>
                    <td class="td-data-dashed" style="width: 20%;" colspan="2"/>
                    <td class="td-data-dashed" style="width: 15%;text-align:right;">${data['date']}</td>
                    <td class="td-data-dashed" style="width: 15%;text-align:right;">${data['transaction']}</td>
                    <td class="td-data-dashed" style="width: 15%;text-align:right;">${data['ref']}</td>
                    <td class="td-data-dashed" style="width: 20%;text-align:right;">${data['amount']}</td>
                    <td class="td-data-dashed" style="width: 15%;text-align:right;">${data['curr_amount']}</td>
                </tr>
                %endif
                %if data['print_header'] == False:
                <tr>
                    <td class="td-data-dashed" style="width: 20%;" colspan="2"/>
                    <td class="td-data-dashed" style="width: 15%;text-align:right;">${data['date']}</td>
                    <td class="td-data-dashed" style="width: 15%;text-align:right;">${data['transaction']}</td>
                    <td class="td-data-dashed" style="width: 15%;text-align:right;">${data['ref']}</td>
                    <td class="td-data-dashed" style="width: 20%;text-align:right;">${data['amount']}</td>
                    <td class="td-data-dashed" style="width: 15%;text-align:right;">${data['curr_amount']}</td>
                </tr>
                %endif
            %endif
            %if data['is_total'] == True:
            <tr>
                <td class="td-data" style="width: 65%;" colspan="5"><b>Totals :</b></th>
                <td class="td-data" style="width: 20%;text-align:right;"><b>${data['amount']}</b></td>
                <td class="td-data" style="width: 15%;text-align:right;"><b>${data['curr_amount']}</b></td>
            </tr>
            %endif
        %endfor
    </table>
    </body>
</html>
