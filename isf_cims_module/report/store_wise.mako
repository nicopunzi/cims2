<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        ${css}
table {
    border-spacing: 0px;
    page-break-inside: auto
}

table tr {
        page-break-inside: avoid; 
        page-break-after: auto;
}
        
@media print {
   thead {display: table-header-group;}
}
        
.break{
    display: block;
    clear: both;
    page-break-after: always;
}

.table-data {
    border: 1px solid black;
    border-collapse: collapse;
    width:100%;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.table-data-nb {
    border: 0px;
    border-collapse: collapse;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.td-data {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.td-data-nb {
    border: 0px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.th-data {
    font-family:verdana;
    font-size:10px;
}


    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print tableheaders()
    print headers()
    print lines()
    """
    %>
    
    <h3>
        Store Wise Report
    </h3>
    
    <center>
        <table class="table-data" style="width: 33%;">
            %for head in headers():
            <tr>
                <th class="td-data" style="width: 33%;text-align:center;">Date</th>
                <td class="td-data" style="width: 33%;text-align:center;">${head['date']}</td>
            </tr>
            %endfor
        </table>
    </center>

    <br/>

    <table class="table-data">
        <tr>
        	<th class="td-data" style="width: 5%;">Code</th>
            <th class="td-data" style="width: 25%;">Product</th>
            %for thead in tableheaders():
                    <th class="td-data" style="width: 15%;">${thead['location']}</th>
            %endfor
            <th class="td-data" style="width: 15%;">Total Qty</th>
            <th class="td-data" style="width: 10%;">Out of Stock</th>
        </tr>
    
        %for data in lines():
            %if data['first_account'] == '1':
                <tr>
                <td class="td-data">${data['code']}</td>
                <td class="td-data">${data['product']}</td>
            %endif
            %if data['is_total'] == '0':   
            	%if data['warning_level'] == True:
                	<td class="td-data" style="color:red;text-align:right">${data['qty']}</td>
                %else:
                	<td class="td-data" style="text-align:right">${data['qty']}</td>
                %endif
            %endif
            %if data['is_total'] == '1':
		        %if data['warning_level'] == True:   
		            <td class="td-data" style="color:red;text-align:right"><b>${data['qty']}</b></td>
		        %else:
		        	<td class="td-data" style="text-align:right"><b>${data['qty']}</b></td>
		        %endif
            %endif
            %if data['is_total'] == '2':
		        %if data['out_of_stock'] == True:
		            <td class="td-data" style="text-align:center">Out of Stock</td>
		        %else:
		        	<td class="td-data" style="text-align:right"></td>
	            %endif
            %endif
        %endfor
        </tr>
    </table>
        
        
</body>
</html>
