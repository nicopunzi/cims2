<html>
<head>
    <style type="text/css">
        ${css}
        
@media print {
   thead {display: table-header-group;}
}
        
.break{
    display: block;
    clear: both;
    page-break-after: always;
}

.table-data {
    border: 1px solid black;
    border-collapse: collapse;
    width:100%;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.table-data-nb {
    border: 0px;
    border-collapse: collapse;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.td-data {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.td-data-nb {
    border: 0px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.th-data {
    font-family:verdana;
    font-size:10px;
}


    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print tableheaders()
    print headers()
    print lines()
    """
    %>
    
    
    <h3>
    Stock Moves Report
    </h3>
   
    <center>    
        <table class="table-data">
            <tr>
                <th class="td-data" style="width: 33%;">Location</th>
                <th class="td-data" style="width: 33%;">Date Start</th>
                <th class="td-data" style="width: 33%;">Date End</th>
            </tr>
            %for head in headers():
                <tr>
                    <td class="td-data" style="width: 33%;">${head['location']}</td>
                    <td class="td-data" style="width: 33%;text-align:right">${head['date_start']}</td>
                    <td class="td-data" style="width: 33%;text-align:right">${head['date_end']}</td>
                </tr>
            %endfor
        </table>
    </center>

    <br/>

    <table class="table-data" >
            <tr>
                <th class="td-data" style="width: 10%;">Code</th>
                <th class="td-data" style="width: 30%;">Product</th>
                <th class="td-data" style="width: 15%;text-align:right;">Opening Stock</th>
                <th class="td-data" style="width: 15%;text-align:right;">Received</th>
                <th class="td-data" style="width: 15%;text-align:right;">Used</th>
                <th class="td-data" style="width: 15%;text-align:right;">Closing Stock</th>
            </tr>
            %for data in lines():
                <tr>
                    <td class="td-data" style="width: 10%;text-align: right;">${data['code']}</td>
                    <td class="td-data" style="width: 30%;">${data['name']}</td>
                    <td class="td-data" style="width: 15%;text-align:right;">${data['open']}</td>
                    <td class="td-data" style="width: 15%;text-align:right;">${data['received']}</td>
                    <td class="td-data" style="width: 15%;text-align:right;">${data['used']}</td>
                    <td class="td-data" style="width: 15%;text-align:right;">${data['close']}</td>
                </tr>
            %endfor
    </table>
</body>
</html>
