# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale
import datetime

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug=False

class isf_stock_internal_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.stock.internal.webkit'

    def __init__(self, cr, uid, name, context=None):
        super(isf_stock_internal_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'headers' : self.headers,
        })
        self.context = context
        self.result_acc = []
        self.headers_acc = []
    
    
    def headers(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.internal.wizard')
        loc_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['location_id'])
        date_start_date = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_start'])
        date_stop_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_stop'])
        date_start = date_start_date[0]['date_start']
        date_stop = date_stop_data[0]['date_stop']
        location_id = loc_data[0]['location_id']
        
        res = {
            'date_start' : date_start,
            'date_stop' : date_stop,
            'location' : location_id[1]
        }
        self.headers_acc.append(res)
        
        
        return self.headers_acc
        
    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.internal.wizard')
        loc_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['location_id'])
        date_start_date = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_start'])
        date_stop_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_stop'])
        date_start = date_start_date[0]['date_start']
        date_stop = date_stop_data[0]['date_stop']
        location_id = loc_data[0]['location_id'][0]
        
        found_pos = False
        total_amount = 0
        # Check if the location is a shop
        pos_o = self.pool.get('isf.pos')
        pos_ids = pos_o.search(self.cr, self.uid, [('state','=','confirm'),('date','>=',date_start),('date','<=',date_stop),('location_id','=',location_id),('product_type_sel','=','product'),('transaction_type_product','=','internal')])
        for pos in pos_o.browse(self.cr, self.uid, pos_ids):
            for line in pos.product_ids:
                found_pos = True
                total_amount += (line.quantity * line.unit_price)
                res = {
                    'is_total' : '0',
                    'date' : pos.date,
                    'tr_id' : pos.sequence_id,
                    'stock' : line.stock_id.name,
                    'product' : line.product_id.name_template,
                    'qty' : line.quantity,
                    'unit_price' : locale.format("%20d",line.unit_price, grouping=True),
                    'total_price' : locale.format("%20d",line.quantity * line.unit_price, grouping=True),
                    'expiry_date' : line.expiry_date,
                }
                
                self.result_acc.append(res)
          
        # Check if is pharmacy pos
        if not found_pos:
            pharm_o = self.pool.get('isf.pharmacy.pos')
            stock_o = self.pool.get('stock.move')
            pharm_ids = pharm_o.search(self.cr, self.uid, [('type','=','internal'),('date','>=',date_start),('date','<=',date_stop),('state','=','close')])
            for pharm in pharm_o.browse(self.cr, self.uid, pharm_ids):
                for line in pharm.product_line_ids:
                    stock_ids = stock_o.search(self.cr, self.uid, [('state','=','done'),('note','=',pharm.sequence_id),('product_id','=',line.product.id)])
                    stock_move = stock_o.browse(self.cr, self.uid, stock_ids)[0]
                    total_amount += (line.price * line.amount)
                    res = {
                        'is_total' : '0',
                        'date' : pharm.date,
                        'tr_id' : pharm.sequence_id,
                        'stock' : stock_move.prodlot_id.name,
                        'product' : line.product.name_template,
                        'qty' : line.amount,
                        'unit_price' : locale.format("%20d",line.price, grouping=True),
                        'total_price' : locale.format("%20d",line.price * line.amount, grouping=True),
                        'expiry_date' : str(stock_move.prodlot_id.life_date)[:10],
                    }
                
                    self.result_acc.append(res)
            
        
        res = {
            'is_total' : '1',
            'total_amount' : locale.format("%20d",total_amount, grouping=True),
        }
        self.result_acc.append(res)
        return self.result_acc
    

report_sxw.report_sxw('report.isf.stock.internal.webkit', 'isf.stock.internal.wizard', 'addons/isf_cims_module/report/stock_internal.mako', parser=isf_stock_internal_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
