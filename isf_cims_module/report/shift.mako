<html>
<head>
    <style type="text/css">
        ${css}
@media print {
   thead {display: table-header-group;}
}
        
.break{
    display: block;
    clear: both;
    page-break-after: always;
}

.table-data {
    border: 1px solid black;
    border-collapse: collapse;
    width:100%;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.table-data-nb {
    border: 0px;
    border-collapse: collapse;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.td-data {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.td-data-nb {
    border: 0px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.th-data {
    font-family:verdana;
    font-size:10px;
}
    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print header()
    print lines()
    """
    %>
    <h3>
    </h3>
    <center>
        <table class="table-data">
            <tr>
                <th class="td-data" style="width: 40%;">Department</th>
                <th class="td-data" style="width: 40%;">Date</th>
            </tr>
            <tr>
            %for head in header():
                <td class="td-data" style="width: 40%;"><b>PHARMACY</b></td>
                <td class="td-data" style="width: 40%;">${head['date']}</td>
            %endfor
            </tr>
        </table>
    </center>
    
    <br/>
    
    <table class="table-data">
            <tr>
                <th class="td-data" style="width: 40%;">Shift</th>
                <th class="td-data" style="width: 40%;">Income</th>
            </tr>
        %for data in lines():
            <tr>
                <td class="td-data" style="width: 40%;">Shift A</td>
                <td class="td-data" style="width: 40%;text-align:right;">${data['total_shift_a']}</td>
            </tr>
            <tr>
                <td class="td-data" style="width: 40%;">Shift B</td>
                <td class="td-data" style="width: 40%;text-align:right;">${data['total_shift_b']}</td>
            </tr>
            <tr>
                <td class="td-data" style="width: 40%;">Shift C</td>
                <td class="td-data" style="width: 40%;text-align:right;">${data['total_shift_c']}</td>
            </tr>
        %endfor
    </table>
</body>
</html>
