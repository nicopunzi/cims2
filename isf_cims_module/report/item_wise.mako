<html>
<head>
    <style type="text/css">
        ${css}
        
@media print {
   thead {display: table-header-group;}
}
        
.break{
    display: block;
    clear: both;
    page-break-after: always;
}

.table-data {
    border: 1px solid black;
    border-collapse: collapse;
    width:100%;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.table-data-nb {
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.td-data {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.td-data-nb {
    border: 0px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.th-data {
    font-family:verdana;
    font-size:10px;
}

.td-data-dashed {
    border: 1px solid black;
    border-collapse: collapse;
    border-style: dashed;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}


    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print headers()
    print lines()
    """
    %>
    <center>
        <h3>Sales Summary - Item Wise Report</h3>
    </center>
    %for data in headers():
    <center>
        <table class="table-data" width="70%">
            <tr>
                <th class="td-data" style="width: 50%;">Date Start</th>
                <th class="td-data" style="width: 50%;">Date Stop</th>
            </tr>
            <tr>
                <td class="td-data" style="width: 50%;text-align:right;">${data['date_start']}</td>
                <td class="td-data" style="width: 50%;text-align:right;">${data['date_end']}</td>
            </tr>
        </table>
    </center>
    %endfor
    <br/>
    <table class="table-data" width="100%">
        <tr>
            <th class="td-data" style="width: 10%;">Type</th>
            <th class="td-data" style="width: 10%;">Group</th>
            <th class="td-data" style="width: 10%;">Code</th>
            <th class="td-data" style="width: 40%;">Product</th>
            <th class="td-data" style="width: 15%;">Quantity</th>
            <th class="td-data" style="width: 15%;">Amount</th>
        </tr>
        %for data in lines():
        <tr>
            %if data['is_header'] == True:
                <td class="td-data-nb" style="width: 100%;" colspan="6"><b>${data['type']} - ${data['location']}</b></th>
                <tr>
                    <td class="td-data-dashed" style="width: 20%;" colspan="2" />
                    <td class="td-data-dashed" style="width: 10%;">${data['code']}</th>
                    <td class="td-data-dashed" style="width: 40%;">${data['product']}</th>
                    <td class="td-data-dashed" style="width: 15%;text-align:right;">${data['sales_qty']}</td>
                    <td class="td-data-dashed" style="width: 15%;text-align:right;">${data['sales']}</td>
                </tr>
            %endif
            %if data['is_total'] == False and data['is_header'] == False:
                <td class="td-data-dashed" style="width: 20%;" colspan="2" />
                <td class="td-data-dashed" style="width: 10%;">${data['code']}</th>
                <td class="td-data-dashed" style="width: 40%;">${data['product']}</th>
                <td class="td-data-dashed" style="width: 15%;text-align:right;">${data['sales_qty']}</td>
                <td class="td-data-dashed" style="width: 15%;text-align:right;">${data['sales']}</td>
            %endif
            %if data['is_total'] == True:
                <td class="td-data" style="width: 70%;" colspan="4"><b>Totals :</b></th>
                <td class="td-data" style="width: 20%;text-align:right;"><b>${data['sales_qty']}</b></td>
                <td class="td-data" style="width: 20%;text-align:right;"><b>${data['sales']}</b></td>
            %endif
        </tr>
        %endfor
    </table>
    </body>
</html>
