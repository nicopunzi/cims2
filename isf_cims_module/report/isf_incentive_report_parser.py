# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale
import platform

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug = False

class isf_incentive_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.incentive.webkit'

    def __init__(self, cr, uid, name, context=None):
        super(isf_incentive_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'header' : self.header,
        })
        self.context = context
        self.result_incentive = []
        self.result_header = []

    
    def header(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.incentive.payslip')
        obj_incentive = self.pool.get('isf.hr.incentive')
        period_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['period_id'])
        grade_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['grade_ids'])
        period = period_data[0]['period_id']
        grade = grade_data[0]['grade_ids']
        
        if grade == 'all':
            grade_str = 'All'
        else:
            grade_id = int(grade)
            grade_obj = self.pool.get('hr.contract.type')
            grade_ids = grade_obj.search(self.cr, self.uid, [('id','=',grade_id)])
            grade_str = grade_obj.browse(self.cr, self.uid, grade_ids)[0].name
        
        res = {
            'period' : period[1],
            'grade' : grade_str,
        }
        
        self.result_header.append(res)
            
        return self.result_header

    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.incentive.payslip')
        obj_incentive = self.pool.get('isf.hr.incentive')
        grade_obj = self.pool.get('hr.contract.type')
        contract_obj = self.pool.get('hr.contract')
        period_obj = self.pool.get('account.period')
        period_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['period_id'])
        grade_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['grade_ids'])
        period = period_data[0]['period_id']
        grade = grade_data[0]['grade_ids']
        period_id = period[0]
        
        grade_id = False
        if grade == 'all':
            grade_str = 'All'
        else:
            grade_id = int(grade)
            
        period_ids = period_obj.search(self.cr, self.uid, [('id','=',period_id)])
        period = period_obj.browse(self.cr, self.uid, period_ids)[0]
        incentive_ids = obj_incentive.search(self.cr, self.uid, [('start_period_id.date_start','<=',period.date_start),('start_period_id.date_stop','>=',period.date_stop)])
        for incentive in obj_incentive.browse(self.cr, self.uid, incentive_ids):
            grade = False
            add_data = False
            contract_ids = contract_obj.search(self.cr, self.uid, [('employee_id','=',incentive.employee_id.id)])
            if len(contract_ids) > 0:
                contract = contract_obj.browse(self.cr,self.uid, contract_ids)[0]
                if contract.type_id:
                    grade = contract.type_id
            
            if not grade_id:
                add_data = True
                if grade:
                    grade_str = grade.name
                else:
                    grade_str = 'N.A.'
            else:
                if grade:
                    if grade.id == grade_id:
                        add_data = True
                        grade_str = grade.name
            code = incentive.employee_id.unique_code
            if not code:
                code = '-'
            if add_data:
                res = {
                    'employee' : incentive.employee_id.name,
                    'code' : code,
                    'amount' : locale.format("%20d",incentive.amount, grouping=True),
                    'currency' : incentive.currency_id.name,
                    'grade' : grade_str,
                }
                self.result_incentive.append(res)         
                        
        self.result_incentive = sorted(self.result_incentive, key=lambda k: k['grade'])
        return self.result_incentive

report_sxw.report_sxw('report.isf.incentive.webkit', 'isf.hr.incentive', 'addons/isf_cims_module/report/incentive.mako', parser=isf_incentive_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
