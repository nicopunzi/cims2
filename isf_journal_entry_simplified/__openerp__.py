# -*- coding: utf-8 -*-

{
	'name': 'ISF Journal Entry Simplified',
	'version': '1.0',
	'category': 'Tools',
	'description': """

ISF Journal Entry Simplified 
==================================

Add a view like account.move.form but with the following columns hidden:

* invoice
* partner_id
* date_maturity
* currency_id
* tax_code_id
* tax_amount
* state
* reconcile_id
* reconcile_partial_id

	
""",
	'author': 'Alessandro Domanico (alessandro.domanico@informaticisenzafrontiere.org)',
	'website': 'www.informaticisenzafrontiere.org',
	'license': 'AGPL-3',
	'depends': ['isf_cims_module'],
	'data': [
			'isf_journal_entry_simplified.xml',
			'data/isf.home.dashboard.action.csv',
    	],
	'demo': [],
	'installable' : True,
}

